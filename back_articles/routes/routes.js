'use strict';
const MongoClient = require('mongodb').MongoClient;
var BDD_NAME = process.env.BDD;

if (process.env.NODE_ENV == 'DEV') BDD_NAME += 'Dev';
if (process.env.NODE_ENV == 'TEST') BDD_NAME += 'Test';
/** File containing all the API's routes */
module.exports = function (app) {

    var Bdd;
    const url = process.env.URL_DB;
    var ArticlesController = require('../controllers/ArticlesController');

    MongoClient.connect(url, { useNewUrlParser: true, useUnifiedTopology: true }).then(function (db) {
        Bdd = db;
    }).catch(err => { console.log(err) });

    app.use(function (req, res, next) {
        req.bdd = Bdd.db(BDD_NAME);
        next();
    });
    app.route('/articles').get(ArticlesController.getArticles);
}

