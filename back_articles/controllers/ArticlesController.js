const axios = require('axios');

var DbArticles = require('../models/DbArticles');

exports.getArticles = (req, res) => {

    var url;
    var filters = req.query.filters;
    DbArticles.getArticles(req.bdd).then((result) => {
        if (result == null) {
            res.status(404)
            res.json({
                message: "Error",
                error: { message: "No Articles found" }
            });
            throw new Error("No Articles found");
        }
        var config = {
            headers: {
                [result.header_var]: result.key_api,
            }
        }
        url = result.url.toString();
        if (filters != null) {
            url += "&categories=" + filters;
        }
        axios.get(url, config).then((resultApi) => {
            res.json(resultApi.data.Data);
        }).catch((reject) => {
            console.log(reject);
        });
    }).catch((reject) => {
        console.log(reject);
    });
}