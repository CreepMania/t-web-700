const request = require('supertest')
const app = require('../app')
var assert = require('assert');

/**
 * Resets the test database before running all tests
 */
before(function(done) {
  const dotenv = require('dotenv').config()
  const url = process.env.URL_DB;
  var bddName = process.env.BDD + "Test";
  var MongoClient = require('mongodb').MongoClient;

  MongoClient.connect(url)
  .then((db) => {return db.db(bddName).collection('Users').remove({})})
  .then(r => done())
  .catch(err => done(err))
})

describe('User', () => {

  /**
   * Test all register functions
   */
  describe('--> REGISTER', async () => {
    it('should register a new user', async () => {
      return request(app)
        .post('/users/register')
        .send({
          email: "test2@test.com",
          password: "test",
          pseudo: "test",
          avatar: "base64"
        })
        .expect(200)
    })
  
    it('should error on register with invalid body', async () => {
      return request(app)
        .post('/users/register')
        .send({})
        .expect(400, JSON.stringify({"message": "Bad Request","error": {"message": "Bad Request"}}))
    });
  
    it('should error register on existing user', async () => {
      return request(app)
        .post('/users/register')
        .send({
          email: "test2@test.com",
          password: "test",
          pseudo: "test",
          avatar: "base64"
        })
        .expect(400, JSON.stringify({"message": "Error","error": {"message": "Mail Already Taken"}}))
    });
  })

  /**
   * Test all login functions
   */
  // bearer jwt token once user is connected
  var loginToken;
  describe('--> LOGIN', async () => {
    it('should log in user', async () => {
      return request(app)
        .post('/users/login')
        .send({
          email: 'test2@test.com',
          password: "test"
        })
        .expect(200)
        .then(res => { loginToken = res.body; assert(res.body.length > 0) })
    })
    
    it('should error on invalid password', async () => {
      return request(app)
        .post('/users/login')
        .send({
          email: 'test2@test.com',
          password: "INVALIDPASSWORD"
        })
        .expect(404, JSON.stringify({"message": "Bad fields","error": {"message": "Bad Password/Email"}}))
    })

    it('should error on empty body', async () => {
      return request(app)
        .post('/users/login')
        .send({})
        .expect(400, JSON.stringify({ message: "Bad Request", error: { message: "Bad Request" }}))
    })
  })

  /**
   * Test all profile functions
   */
  describe('--> PROFILE', async () => {
    it('should not be able to get user info when not connected', async () => {
      return request(app)
        .get('/users/profile')
        .expect(401, JSON.stringify({"message": "Error","error": {"message": "Bad or missing token"}}))
    })
  
    it('should not be able to get user info with bad token', async () => {
      return request(app)
        .get('/users/profile')
        .set('Authorization', 'Bearer ' + loginToken.slice(0, -1))
        .expect(401, JSON.stringify({"message": "Error","error": {"message": "Bad or missing token"}}))
    })

    it('should get user info', async () => {
      return request(app)
        .get('/users/profile')
        .set('Authorization', 'Bearer ' + loginToken)
        .expect(200, JSON.stringify({"email":"test2@test.com","pseudo":"test","avatar":"base64","admin":false,"defaultCurrency":1,"cryptocurrencies":[],"filters":[]}))
    })

    it('should error on bad body', async () => {
      return request(app)
        .put('/users/profile')
        .send({})
        .set('Authorization', 'Bearer ' + loginToken)
        .expect(400, JSON.stringify({"message": "Bad Request","error": {"message": "Bad Request"}}))
      })

    it('should 304 on unmodified user', async () => {
      return request(app)
        .put('/users/profile')
        .set('Authorization', 'Bearer ' + loginToken)
        .send({
          "email": "test2@test.com",
          "pseudo": "test",
          "avatar": "base64",
          "admin": false,
          "defaultCurrency": 1,
          "cryptocurrencies": [],
          "filters": []
        })
        .expect(304)
    })
  
    it('should modify user', async () => {
      return request(app)
        .put('/users/profile')
        .set('Authorization', 'Bearer ' + loginToken)
        .send({
          "email": "prout@test.com",
          "pseudo": "test",
          "avatar": "base64",
          "admin": false,
          "defaultCurrency": 1,
          "cryptocurrencies": [],
          "filters": []
        })
        .expect(200)
    })
  
    it('should get modified user info', async () => {
      return request(app)
        .get('/users/profile')
        .set('Authorization', 'Bearer ' + loginToken)
        .expect(200, JSON.stringify({"email":"prout@test.com","pseudo":"test","avatar":"base64","admin":false,"defaultCurrency":1,"cryptocurrencies":[],"filters":[]}))
    })
  })

  /**
   * Test all logout functions
   */
  describe('--> LOGOUT', async () => {
    it('should not logout when no token is set', async () => {
      return request(app)
        .post('/users/logout')
        .expect(401, JSON.stringify({"message": "Error","error": {"message": "Bad or missing token"}}))
    })
  
    it('should logout', async () => {
      return request(app)
        .post('/users/logout')
        .set('Authorization', 'Bearer ' + loginToken)
        .expect(200)
    })
  })
  
})

