var express = require('express');
var jwt = require('jsonwebtoken');

exports.checkToken = (req, res, next) => {
    try {
        var token = req.headers.authorization.split(" ")[1];
        jwt.verify(token, process.env.SECRETJWT, (err, decoded) => {
            if (err) {
                return res.status(401).json({
                    message: "Error",
                    error: { message: "Bad or missing token" }
                });
            } else {
                req._id = decoded._id;
                req.admin = decoded.admin;
                next();
            }
        });
    } catch (e) {
        return res.status(401).json({
            message: "Error",
            error: { message: "Bad or missing token" }
        });
    }
}

exports.checkTokenAdmin = (req, res, next) => {
    try {
        var token = req.headers.authorization.split(" ")[1];
        jwt.verify(token, process.env.SECRETJWT, (err, decoded) => {
            if (err) {
                return res.status(401).json({
                    message: "Error",
                    error: { message: "Bad or missing token" }
                });
            } else {
                if (decoded.admin == true) {
                    return res.status(200).json(true);
                }
                return res.status(403).json({
                    message: "Error",
                    error: { message: "Forbidden" }
                });
            }
        });
    } catch (e) {
        return res.status(401).json({
            message: "Error",
            error: { message: "Bad or missing token" }
        });
    }
}



exports.checkTokenUsers = (req, res, next) => {
    try {
        var token = req.headers.authorization.split(" ")[1];
        jwt.verify(token, process.env.SECRETJWT, (err, decoded) => {
            if (err) {
                return res.status(401).json({
                    message: "Error",
                    error: { message: "Bad or missing token" }
                });
            } else {
                    return res.status(200).json(true);
            }
        });
    } catch (e) {
        return res.status(401).json({
            message: "Error",
            error: { message: "Bad or missing token" }
        });
    }
}