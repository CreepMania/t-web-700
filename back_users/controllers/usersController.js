var jwt = require('jsonwebtoken');
const { google } = require('googleapis');
var OAuth2Data;
if (process.env.NODE_ENV == 'DEV' || false) {
  OAuth2Data = require('../google_secret.dev.json')
} else {
  OAuth2Data = require('../google_secret.json')
}

const CLIENT_ID = OAuth2Data.web.client_id;
const CLIENT_SECRET = OAuth2Data.web.client_secret;
const REDIRECT_URL = OAuth2Data.web.redirect_uris[0];
const oAuth2Client = new google.auth.OAuth2(CLIENT_ID, CLIENT_SECRET, REDIRECT_URL)

var DBUser = require('../models/DBUsers');

var EnumCurrency = {
  'Euro': 1,
  'Dollar': 2
}

exports.login = (req, res) => {
  if (req.body.email == "" || req.body.email == null ||
    req.body.password == "" || req.body.password == null) {
    return res.status(400).json({
      message: "Bad Request",
      error: { message: "Bad Request" }
    });
  }

  DBUser.GetOneUser(req.bdd, req.body.email).then((result) => {
    if (result == null || result.password != req.body.password) {
      res.status(404)
      res.json({
        message: "Bad fields",
        error: { message: "Bad Password/Email" }
      });
      return;
    }
    var token = jwt.sign({ admin: result.admin, _id: result._id }, process.env.SECRETJWT, { expiresIn: 60 * 60 * 24 });
    res.json(token);
  }).catch(err => { });
}

exports.register = (req, res) => {
  if (req.body.email == "" || req.body.email == null ||
    req.body.password == "" || req.body.password == null) {
    res.status(400)
    res.json({
      message: "Bad Request",
      error: { message: "Bad Request" }
    });
    return;
  }
  DBUser.GetOneUser(req.bdd, req.body.email)
    .then(result => {
      if (result != null) {
        res.status(400);
        res.json({
          message: "Error",
          error: { message: "Mail Already Taken" }
        });
        throw new Error("Mail Already Taken");
      }
    }).then(function (e) {
      DBUser.Inscription(req.bdd, req.body).then(result => {
        var token = jwt.sign({ admin: result.admin, _id: result.ops[0]._id }, process.env.SECRETJWT, { expiresIn: 60 * 60 * 24 });
        res.json(token);
      }).catch(err => {
        res.json(err);
        res.status(401);
      })
    }).catch((err) => { });

}

exports.log_out = (req, res) => {
  res.json("ok");
}

exports.profil = (req, res) => {
  DBUser.GetProfil(req.bdd, req._id).then(result => {
    res.json(result);
  }).catch(err => {
    res.json(err);
    res.status(404);
  });
}

exports.profilUpdate = (req, res) => {
  if (req.body.email == "" || req.body.email == null) {
    res.status(400)
    res.json({
      message: "Bad Request",
      error: { message: "Bad Request" }
    });
    return;
  }
  if (req.body.cryptocurrencies == null || req.body.cryptocurrencies == '')
    req.body.cryptocurrencies = new Array();
  if (req.body.filters == null || req.body.filters == '')
    req.body.filters = new Array();
  req.body.defaultCurrency = parseInt(req.body.defaultCurrency);
  if (!isNaN(req.body.defaultCurrency)
    && Object.values(EnumCurrency).includes(req.body.defaultCurrency)) {

    DBUser.UpdateProfil(req.bdd, req._id, req.body).then(result => {

      if (result.result.nModified == 0) {
        return res.status(304).send();
      }

      return res.status(200).send()
    })
      .catch(err => {
        res.json(err);
        res.status(404);
      });
    return;
  }
  res.json({
    message: "Error",
    error: { message: "Bad fields" }
  });
  res.status(400);
}

exports.loginGoogle = (req, res) => {
  const url = oAuth2Client.generateAuthUrl({
    access_type: 'offline',
    scope: 'https://www.googleapis.com/auth/userinfo.profile https://www.googleapis.com/auth/userinfo.email'
  });

  res.send('"' + url + '"');
}

exports.googleCallback = (req, res) => {
  const code = req.query.code

  if (code) {
    // Get an access token based on our OAuth code
    oAuth2Client.getToken(code, function (err, tokens) {
      if (err) {
        console.log('Error authenticating')
        console.log(err);
      } else {
        console.log('Successfully authenticated');

        var token = tokens.id_token;
        var decodedToken = jwt.decode(token);

        oAuth2Client.setCredentials(tokens);

        DBUser.GetOneUser(req.bdd, decodedToken.email)
          .then(result => {
            console.log(result);

            if (result != null) {
              var token = jwt.sign({ admin: result.admin, _id: result._id }, process.env.SECRETJWT, { expiresIn: 60 * 60 * 24 });
              return res.status(200).json(token);
            } else {
              var body = { email: decodedToken.email, password: null, pseudo: decodedToken.given_name, avatar: decodedToken.picture }
              DBUser.Inscription(req.bdd, body).then(resultI => {
                var token = jwt.sign({ admin: resultI.admin, _id: resultI.ops[0]._id }, process.env.SECRETJWT, { expiresIn: 60 * 60 * 24 });
                return res.json(token);
              }).catch(err => {
                return res.status(401).json(err);
              })
            }
          }).catch((err) => { console.error(err) });
      }
    });
  }
}