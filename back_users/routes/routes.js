'use strict';
const MongoClient = require('mongodb').MongoClient;
var BDD_NAME = process.env.BDD;

if (process.env.NODE_ENV == 'DEV') BDD_NAME += 'Dev';
if (process.env.NODE_ENV == 'TEST') BDD_NAME += 'Test';


/** File containing all the API's routes */
module.exports = function (app) {

    var userController = require('../controllers/usersController');
    var tokenController = require('../controllers/TokenController');
    var dbConnection;
    const url = process.env.URL_DB;

    MongoClient.connect(url, { useNewUrlParser: true, useUnifiedTopology: true }).then(function (db) {
        dbConnection = db;
    }).catch(err => { console.log(err) });

    app.use(function (req, res, next) {
        req.bdd = dbConnection.db(BDD_NAME);
        //res.setHeader('Access-Control-Allow-Origin', '*');
        next();
    });

    app.route('/users/login').post(userController.login);
    app.route('/users/register').post(userController.register);
    app.route('/users/logout').post(tokenController.checkToken, userController.log_out);
    app.route('/users/profile').get(tokenController.checkToken, userController.profil);
    app.route('/users/profile').put(tokenController.checkToken, userController.profilUpdate);
    app.route('/users/tokenAdmin').get(tokenController.checkTokenAdmin);
    app.route('/users/tokenUsers').get(tokenController.checkTokenUsers);
    app.route('/users/auth/google').get(userController.loginGoogle);
    app.route('/users/auth/google/callback').get(userController.googleCallback);
}

