const ObjectId = require('mongodb').ObjectID;

var DBUsers = {

    GetAllUsers:function (bdd,callback) {
        return bdd.collection("Users").find().toArray(callback);
    },

    GetOneUser:function (bdd, email) {
      return bdd.collection("Users").findOne({email:email});
    },

    GetProfil:function(bdd,_id){
      return bdd.collection("Users").findOne({_id:new ObjectId(_id)},{projection:{_id: 0, email:1 , pseudo: 1, avatar: 1, admin: 1, defaultCurrency: 1, cryptocurrencies: 1, filters:1 }});
    },

    Inscription:function (bdd, body) {
      return bdd.collection("Users").insertOne({email: body.email, password: body.password, pseudo: body.pseudo, avatar: body.avatar,
         admin: false, defaultCurrency: 1, cryptocurrencies: new Array(), filters:new Array(), token: "", timestamp: ""});
    },

    UpdateProfil(bdd, _id, body){
      return bdd.collection("Users").updateOne({_id:new ObjectId(_id)},{$set:{email: body.email , pseudo: body.pseudo, avatar: body.avatar, defaultCurrency: body.defaultCurrency, cryptocurrencies: body.cryptocurrencies, filters:body.filters }});
    }

}
module.exports = DBUsers;