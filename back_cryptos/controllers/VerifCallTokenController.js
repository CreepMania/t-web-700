const axios = require('axios');

var urlApiUser;
if (process.env.NODE_ENV == 'DEV') {
    urlApiUser = process.env.URL_API_DEV_USER;
} else {
    urlApiUser = process.env.URL_API_PROD_USER;
}

exports.checkTokenAdminAllCrypto = (req, res, next) => {

    var config = {
        headers: {
            authorization: req.headers.authorization,
        }
    }
    axios.get(urlApiUser + "users/tokenAdmin", config).then((resultApi) => {
        req.isAdmin = true;
        next();
    }).catch((error) => {
        req.isAdmin = false;
        next();
    });
}

exports.checkTokenAdmin = (req, res, next) => {
    var config = {
        headers: {
            authorization: req.headers.authorization,
        }
    }
    axios.get(urlApiUser + "users/tokenAdmin", config).then((resultApi) => {
        next();
    }).catch((error) => {
        res.status(403).json({
            message: "Error",
            error: { message: "Forbidden" }
        });
    });
}

exports.checkTokenUsers = (req, res, next) => {
    var config = {
        headers: {
            authorization: req.headers.authorization,
        }
    }
    axios.get(urlApiUser + "users/tokenUsers", config).then((resultApi) => {
        next();
    }).catch((error) => {
        res.status(403).json({
            message: "Error",
            error: { message: "Forbidden" }
        });
    });
}