const axios = require('axios');

var DbCrypto = require('../models/DbCryptos');

exports.getAllCryptos = (req, res) => {
    var cmids = req.query.cmids;
    var tabCmids;
    if (cmids != null) {
        tabCmids = cmids.split(',');
    }
    DbCrypto.getCryptos(req.bdd, tabCmids, req.isAdmin).then((result) => {
        if (result == null || result.length <= 0)
            throw new Error("No crypto found");
        var bddCrypto = "";
        if (cmids == null) {
            for (var cpt = 0; cpt < result.length; cpt++) {
                bddCrypto += result[cpt].shortName + ",";
            }
            bddCrypto = bddCrypto.substr(0, bddCrypto.length - 1);
        } else {
            bddCrypto = cmids;
        }
        var url = process.env.URL_API + "pricemulti?fsyms=" + bddCrypto + "&tsyms=USD,EUR&api_key=" + process.env.KEY_API;
        axios.get(url).then((resultApi) => {
            if (resultApi.data.Response == "Error")
                throw new Error("Error Api");
            for (var cpt = 0; cpt < result.length; cpt++) {
                try {
                    result[cpt].eur = resultApi.data[result[cpt].shortName].EUR;
                    result[cpt].usd = resultApi.data[result[cpt].shortName].USD;
                } catch (e) {
                    result[cpt].eur = null;
                    result[cpt].usd = null;
                }
            }
            res.json(result);
        }).catch(err => {
            console.log(err);
            res.status(404).json({
                message: "Error",
                error: { message: "Error Api" }
            });
        });
    }).catch(err => {
        console.log(err);
        res.status(404).json({
            message: "Error",
            error: { message: "No crypto found" }
        });
    });
}


exports.getCrypto = (req, res) => {
    var crypto = req.params.cmid
    DbCrypto.getCrypto(req.bdd, crypto).then((result) => {
        if (result == null)
            throw new Error("No crypto found");
        var url = process.env.URL_API + "pricemulti?fsyms=" + crypto + "&tsyms=USD,EUR&api_key=" + process.env.KEY_API;
        axios.get(url).then((resultApi) => {
            if (resultApi.data.Response == "Error")
                throw new Error("Bug Api");
            try {
                result.eur = resultApi.data[crypto].EUR;
                result.usd = resultApi.data[crypto].USD;
            } catch (e) {
                result[cpt].eur = null;
                result[cpt].usd = null;
            }
            res.json(result);
        }).catch(err => {
            console.log(err);
            res.status(404).json({
                message: "Error",
                error: { message: "Problem with the api" }
            });
        });
    }).catch(err => {
        console.log(err);
        res.status(404).json({
            message: "Error",
            error: { message: "No crypto found" }
        });
    });
}

exports.getCryptoHistory = (req, res) => {
    var crypto = req.params.cmid;
    var period = req.params.period;
    var defaultcurrencies = req.query.defaultcurrencies;
    var tabPeriod = [["d", "day"], ["h", "hour"], ["m", "minute"]];
    var MapPeriod = new Map(tabPeriod);
    if (defaultcurrencies == "" || defaultcurrencies == null)
        defaultcurrencies = "EUR"

    DbCrypto.getCrypto(req.bdd, crypto).then((result) => {
        if (result == null)
            throw new Error("No crypto found");
        var typeTime = period.substr(period.length - 1, period.length);
        period = period.substr(0, period.length - 1);
        var search = MapPeriod.get(typeTime);
        var url = process.env.URL_API + "v2/histo" + search + "?fsym=" + crypto + "&tsym=" + defaultcurrencies + "&limit=" + period + "&api_key=" + process.env.KEY_API;
        axios.get(url).then((resultApi) => {
            if (resultApi.data.Response == "Error")
                throw new Error("Bug Api");
            result.data = resultApi.data.Data;
            res.json(result);
        }).catch(err => {
            console.log(err);
            res.status(404).json({
                message: "Error",
                error: { message: "Problem with the api" }
            });
        });
    }).catch(err => {
        console.log(err);
        res.status(404).json({
            message: "Error",
            error: { message: "No crypto found" }
        });
    });
}

exports.addCrypto = (req, res) => {
    DbCrypto.addCrypto(req.bdd, req.body).then((result) => {
        res.status(200).json(result.ops[0]);
    }).catch((err) => {
        res.status(500).json({
            message: "Error",
            error: { message: "Internal Server Error" }
        });
    });
}

exports.modifyCrypto = (req, res) => {
    DbCrypto.modifyCrypto(req.bdd, req.params.cmid, req.body).then((result) => {
        if (result.result.nModified == 0) {
            return res.status(304).send();
        }
        res.status(200).json(true);
    }).catch((err) => {
        console.log(err);
        res.status(500).json({
            message: "Error",
            error: { message: "Internal Server Error" }
        });
    });
}

exports.deleteCrypto = (req, res) => {
    DbCrypto.deleteCrypto(req.bdd, req.params.cmid).then((result) => {
        res.status(200).json(true);
    }).catch((err) => {
        console.log(err);
        res.status(500).json({
            message: "Error",
            error: { message: "Internal Server Error" }
        });
    });
}