const request = require('supertest')
const app = require('../app')
var assert = require('assert');
var jwt = require('jsonwebtoken');

before(function (done) {
    const dotenv = require('dotenv').config()
    const url = process.env.URL_DB;
    var bddName = process.env.BDD + "Test";
    var MongoClient = require('mongodb').MongoClient;

    MongoClient.connect(url, { useNewUrlParser: true, useUnifiedTopology: true })
        .then((db) => { return db.db(bddName).collection('Cryptos').remove({}) })
        .then(r => done())
        .catch(err => done(err))
});

describe('Crypto', () => {

    describe('--> back Crypto for unknow user', async () => {

        it('should GetAllCryptos unknow user', async () => {
            return request(app)
                .get('/cryptos')
                .expect(200)
        });

        it('should GetCryptos with filters unknow user', async () => {
            return request(app)
                .get('/cryptos?cmids=ETH,BTC')
                .expect(200)
        });

        it('should error on GetOneCrypto unknow user', async () => {
            return request(app)
                .get('/cryptos/BTC')
                .expect(403)
        });

        it('should error on GetOneCryptoHistory unknow user', async () => {
            return request(app)
                .get('/cryptos/BTC/history/60d')
                .expect(403)
        });

        it('should error on AddOneCrytpo unknow user', async () => {
            return request(app)
                .post('/cryptos')
                .expect(403)
        });

        it('should error on ModifyOneCrytpo unknow user', async () => {
            return request(app)
                .put('/cryptos/5e1d920d1c9d4400008684ce')
                .send({
                    name: "TRON",
                    logo: "https://s2.coinmarketcap.com/static/img/coins/200x200/1958.png?_=6f9d24a",
                    shortName: "TRX",
                    enable: false
                })
                .expect(403)
        });

        it('should error on DeleteOneCrypto unknow user', async () => {
            return request(app)
                .delete('/cryptos/5e1d920d1c9d4400008684ce')
                .expect(403)
        });
    });

    describe('--> back Crypto for user auth', async () => {

        var token = jwt.sign({ admin: false, _id: "5e26fbd9a4270a779cd6a33a" }, process.env.SECRETJWT, { expiresIn: 60 * 60 * 24 });
        it('should GetAllCryptoInBase user auth', async () => {
            return request(app)
                .get('/cryptos')
                .set("Authorization", "Bearer " + token)
                .expect(200)
        });
        it('should GetCryptos with filters user auth', async () => {
            return request(app)
                .get('/cryptos?cmids=ETH,BTC')
                .set("Authorization", "Bearer " + token)
                .expect(200)
        });

        it('should GetOneCrypto user auth', async () => {
            return request(app)
                .get('/cryptos/BTC')
                .set("Authorization", "Bearer " + token)
                .expect(200)
        });

        it('should GetOneCryptoHistory user auth', async () => {
            return request(app)
                .get('/cryptos/BTC/history/60d')
                .set("Authorization", "Bearer " + token)
                .expect(200)
        });

        it('should error on AddOneCrytpo user auth', async () => {
            return request(app)
                .post('/cryptos')
                .set("Authorization", "Bearer " + token)
                .send({
                    name: "TRON",
                    logo: "https://s2.coinmarketcap.com/static/img/coins/200x200/1958.png?_=6f9d24a",
                    shortName: "TRX"
                })
                .expect(403)
        });

        it('should error on ModifyOneCrytpo user auth', async () => {
            return request(app)
                .put('/cryptos/5e1d920d1c9d4400008684ce')
                .set("Authorization", "Bearer " + token)
                .send({
                    name: "TRON",
                    logo: "https://s2.coinmarketcap.com/static/img/coins/200x200/1958.png?_=6f9d24a",
                    shortName: "TRX",
                    enable: false
                })
                .expect(403)
        });

        it('should error on DeleteOneCrypto user auth', async () => {
            return request(app)
                .delete('/cryptos/5e1d920d1c9d4400008684ce')
                .set("Authorization", "Bearer " + token)
                .expect(403)
        });
    });

    describe('--> back Crypto for admin', async () => {

        var token = jwt.sign({ admin: true, _id: "5e26fbd9a4270a779cd6a33a" }, process.env.SECRETJWT, { expiresIn: 60 * 60 * 24 });
        var idCrypto;

        it('should GetAllCryptoInBase admin', async () => {
            return request(app)
                .get('/cryptos')
                .set("Authorization", "Bearer " + token)
                .expect(200)
        });

        it('should GetOneCrypto admin', async () => {
            return request(app)
                .get('/cryptos/BTC')
                .set("Authorization", "Bearer " + token)
                .expect(200)
        });

        it('should GetOneCryptoHistory admin', async () => {
            return request(app)
                .get('/cryptos/BTC/history/60d')
                .set("Authorization", "Bearer " + token)
                .expect(200)
        });

        it('should AddOneCrytpo admin', async () => {
            return request(app)
                .post('/cryptos')
                .set("Authorization", "Bearer " + token)
                .send({
                    name: "TRON",
                    logo: "https://s2.coinmarketcap.com/static/img/coins/200x200/1958.png?_=6f9d24a",
                    shortName: "TRX"
                })
                .expect(200)
                .then(res => { idCrypto = res.body._id })
        });

        it('should error on ModifyOneCrytpo admin', async () => {
            return request(app)
                .put('/cryptos/' + idCrypto)
                .set("Authorization", "Bearer " + token)
                .send({
                    name: "TRON",
                    logo: "https://s2.coinmarketcap.com/static/img/coins/200x200/1958.png?_=6f9d24a",
                    shortName: "TRX",
                    enable: false
                })
                .expect(200)
        });

        it('should error on DeleteOneCrypto admin', async () => {
            return request(app)
                .delete('/cryptos/' + idCrypto)
                .set("Authorization", "Bearer " + token)
                .expect(200)
        });
    });

    describe('--> back Crypto Error', async () => {

        var token = jwt.sign({ admin: false, _id: "5e26fbd9a4270a779cd6a33a" }, process.env.SECRETJWT, { expiresIn: 60 * 60 * 24 });

        it('should error GetOneCrypto No crypto found', async () => {
            return request(app)
                .get('/cryptos/ACC')
                .set("Authorization", "Bearer " + token)
                .expect(404)
        });

        it('should error GetOneCryptoHistory No crypto found', async () => {
            return request(app)
                .get('/cryptos/ACC/history/60d')
                .set("Authorization", "Bearer " + token)
                .expect(404)
        });

        it('should error GetOneCryptoHistory Invalide date', async () => {
            return request(app)
                .get('/cryptos/ACC/history/XXX')
                .set("Authorization", "Bearer " + token)
                .expect(404)
        });
    });
});