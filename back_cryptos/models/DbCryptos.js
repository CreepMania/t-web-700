const ObjectId = require('mongodb').ObjectID;

var DbCryptos = {

    getCryptos: function (bdd, ArrayCrypto, isAdmin) {
        if (isAdmin == true)
            return bdd.collection("Cryptos").find().toArray();
        if (ArrayCrypto != null)
            return bdd.collection("Cryptos").find({ shortName: { $in: ArrayCrypto }, enable: true }).toArray();
        return bdd.collection("Cryptos").find({ enable: true }).toArray();
    },

    getCrypto: function (bdd, crypto) {
        return bdd.collection("Cryptos").findOne({ shortName: crypto });
    },

    addCrypto: function (bdd, body) {
        return bdd.collection("Cryptos").insertOne({ name: body.name, logo: body.logo, shortName: body.shortName, enable: true });
    },

    modifyCrypto: function (bdd, _id, body) {
        return bdd.collection("Cryptos").updateOne({ _id: new ObjectId(_id) }, { $set: { name: body.name, logo: body.logo, shortName: body.shortName, enable: body.enable /*(body.enable === 'true')*/ } });
    },

    deleteCrypto: function (bdd, _id) {
        return bdd.collection("Cryptos").deleteOne({ _id: new ObjectId(_id) });
    },
}
module.exports = DbCryptos;