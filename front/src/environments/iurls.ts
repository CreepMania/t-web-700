import { ApiUrl } from "src/environments/iapiurl";

/**
 * Class to contains all api url and theirs port.
 */
export class ApiUrls {

    /**
     * Enum, when you try to add a new api, first add on this enum.
     */
    public key: ApiUrl;

    /**
     * Url of api without "/" (ex: http://localhost or https://cryptowebepitech.herokuapp.com).
     */
    public url: string;

    /**
     * Port number (default 80 if no port specify).
     */
    public port: number;

    /**
     * Constructor of ApiUrls class.
     * @param obj new object to build.
     */
    constructor(obj: ApiUrls) {
        this.key = obj.key;
        this.url = obj.url;
        this.port = obj.port;
    }
}
