import { ApiUrl } from "src/environments/iapiurl";
import { ApiUrls } from "src/environments/iurls";

/**
 * Variable to contains all environment variable :
 * - production is always set to true for this file;
 * - api_urls contains all ApiUrls enable on application
 *
 * /!\ If you declare one here, you might declare too on ./environment.ts /!\
 * This is the production environment file, it is used by specify --prod flag, instead, ./environment.ts will be used.
 */
export const environment = {
    production: true,
    api_urls: [
        new ApiUrls({
            key: ApiUrl.Users,
            url: "https://cryptowebepitech.herokuapp.com",
            port: 80
        }),
        new ApiUrls({
            key: ApiUrl.Crypto,
            url: "https://cryptowebepitech-cryptos.herokuapp.com",
            port: 80
        }),
        new ApiUrls({
            key: ApiUrl.Articles,
            url: "https://cryptowebepitech-news.herokuapp.com",
            port: 80
        })
    ]
};
