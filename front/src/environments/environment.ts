import { ApiUrl } from "src/environments/iapiurl";
import { ApiUrls } from "src/environments/iurls";

// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

/**
 * Variable to contains all environment variable :
 * - production is always set to false for this file;
 * - api_urls contains all ApiUrls enable on application
 *
 * /!\ If you declare one here, you might declare too on ./environment.prod.ts /!\
 * This is the default environment file used, it is everrided by --prod flag (./environment.prod.ts used).
 */
export const environment = {
    production: false,
    api_urls: [
        new ApiUrls({
            key: ApiUrl.Users,
            url: "http://localhost",
            port: 3000
        }),
        new ApiUrls({
            key: ApiUrl.Crypto,
            url: "http://localhost",
            port: 3001
        }),
        new ApiUrls({
            key: ApiUrl.Articles,
            url: "http://localhost",
            port: 3002
        })
    ]
};
