import { CommonModule } from "@angular/common";
import { HTTP_INTERCEPTORS, HttpClientModule } from "@angular/common/http";
import { NgModule } from "@angular/core";
import { HeaderComponent } from "src/app/common/header/header.component";
import { ErrorInterceptor } from "src/app/common/interceptors/error.interceptor";
import { JwtInterceptor } from "src/app/common/interceptors/jwt.interceptor";
import { AdminGuard } from "src/app/common/services/admin.guard";
import { ArticlesService } from "src/app/common/services/articles.service";
import { AuthGuard } from "src/app/common/services/auth.guard";
import { AuthUnguard } from "src/app/common/services/auth.unguard";
import { CryptosService } from "src/app/common/services/cryptos.service";
import { UserService } from "src/app/common/services/users.service";

@NgModule({
    exports: [
        HeaderComponent
    ],
    declarations: [
        HeaderComponent
    ],
    providers: [
        UserService,
        ArticlesService,
        CryptosService,
        AuthGuard,
        AuthUnguard,
        AdminGuard,
        { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
        { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
    ],
    imports: [
        HttpClientModule,
        CommonModule
    ]
})

export class AppCommonModule { }
