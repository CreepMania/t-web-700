import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { GuidedTourService } from "ngx-guided-tour";
import { ToastrService } from "ngx-toastr";
import { HelperComponent } from "src/app/common/helper/helper.component";
import { User } from "src/app/common/interfaces/iusers";
import { UserService } from "src/app/common/services/users.service";

/**
 * Header of application (nav bar).
 */
@Component({
    selector: "app-header",
    templateUrl: "./header.component.html",
    styleUrls: ["./header.component.scss"]
})

export class HeaderComponent extends HelperComponent implements OnInit {

    /**
     * Contains logged user.
     */
    public user: User = null;

    /**
     * Constructor of HeaderComponent.
     * @param userService Service of users to get user state and disconnect.
     */
    constructor(private userService: UserService, private toastr: ToastrService, private router: Router, guidedTourService: GuidedTourService) {
        super(guidedTourService);
    }

    /**
     * Method handled at component initialisation.
     */
    public ngOnInit() {
        this.userService.getUserState().subscribe((u: User) => {
            this.user = u;
        });
    }

    /**
     * Method handled when use click on "Logout" link.
     */
    public disconnectUser() {
        this.userService.deleteSession();
        this.userService.logoutUser().subscribe();
        this.toastr.success("You have successfully logged out.", "Disconnection");
        this.router.navigateByUrl("home");
    }

}
