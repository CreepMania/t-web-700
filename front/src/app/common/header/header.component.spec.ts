import { CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";
import { TestBed } from "@angular/core/testing";
import { Router } from "@angular/router";
import { isEqual } from "lodash";
import { GuidedTourService } from "ngx-guided-tour";
import { ToastrService } from "ngx-toastr";
import { of } from "rxjs";
import { HeaderComponent } from "src/app/common/header/header.component";
import { Cryptocurrencies } from "src/app/common/interfaces/icryptocurrencies";
import { Currency, User } from "src/app/common/interfaces/iusers";
import { UserService } from "src/app/common/services/users.service";
import { instance, mock } from "ts-mockito";

let userServiceMock: UserService;
let routerMock: Router;
let toastrServiceMock: ToastrService;

describe("HeaderComponent", () => {

    let component: HeaderComponent;
    let userService: UserService;

    beforeEach(() => {
        userServiceMock = mock(UserService);
        routerMock = mock(Router);
        toastrServiceMock = mock(ToastrService);

        TestBed.configureTestingModule({
            providers: [
                HeaderComponent,
                { provide: UserService, useValue: instance(userServiceMock) },
                { provide: Router, useValue: instance(routerMock) },
                { provide: ToastrService, useValue: instance(toastrServiceMock) },
                { provide: GuidedTourService }
            ],
            schemas: [CUSTOM_ELEMENTS_SCHEMA]
        }).compileComponents();

        component = TestBed.get(HeaderComponent);
        userService = TestBed.get(UserService);
    });

    it("should create", async () => {
        expect(component).toBeTruthy();
    });

    it("should run #ngOnInit() with logged user", async () => {
        const user = new User({
            _id: "",
            email: "test.test@epitech.eu",
            password: "",
            avatar: "",
            pseudo: "Test",
            admin: false,
            cryptocurrencies: [
                new Cryptocurrencies(null)
            ],
            defaultCurrency: Currency.EUR,
            filters: [],
            timestamp: "",
            token: ""
        });
        spyOn(userService, "getUserState").and.returnValue(of(user));
        component.ngOnInit();
        expect(isEqual(component.user, user)).toBeTruthy();
    });

    it("should run #disconnectUser", async () => {
        spyOn(userService, "logoutUser").and.returnValue(of(null));
        component.disconnectUser();
        expect(component.user === null).toBeTruthy();
    });

});
