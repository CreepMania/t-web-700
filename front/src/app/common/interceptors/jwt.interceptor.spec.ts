import { TestBed } from "@angular/core/testing";
import { JwtInterceptor } from "src/app/common/interceptors/jwt.interceptor";
import { UserService } from "src/app/common/services/users.service";
import { instance, mock } from "ts-mockito";

let userServiceMock: UserService;

describe("JwtInterceptor", () => {
    let service: JwtInterceptor;
    let userService: UserService;

    beforeEach(() => {
        userServiceMock = mock(UserService);

        TestBed.configureTestingModule({
            providers: [
                JwtInterceptor,
                { provide: UserService, useValue: instance(userServiceMock) },
            ]
        }).compileComponents();

        service = TestBed.get(JwtInterceptor);
        userService = TestBed.get(UserService);
    });

    it("should create", async () => {
        expect(service).toBeTruthy();
    });

});
