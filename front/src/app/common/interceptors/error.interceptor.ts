import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable, throwError } from "rxjs";
import { catchError } from "rxjs/operators";
import { UserService } from "src/app/common/services/users.service";

/**
 * Interceptor for all http errors.
 */
@Injectable()

export class ErrorInterceptor implements HttpInterceptor {

    /**
     * Constructor of interceptor.
     * @param userService user service to log out user if api return 401.
     */
    constructor(private userService: UserService) { }

    /**
     * Default overrided method.
     * @param request Request intercepted.
     * @param next Next request.
     */
    public intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(request).pipe(catchError((err) => {
            if (err.status === 401 && this.userService.loggedUser !== null) {
                this.userService.logoutUser().subscribe((s) => {
                    this.userService.setSession(null);
                    this.userService.deleteSession();
                });
                location.reload(true);
            }

            const error = err.error.message || err.statusText;
            return throwError(error);
        }));
    }

}
