import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { UserService } from "src/app/common/services/users.service";

/**
 * Interceptor of all http request to add JSON Web Token (JWT).
 */
@Injectable()

export class JwtInterceptor implements HttpInterceptor {

    /**
     * Constructor of interceptor.
     * @param userService user service to check if user is logged.
     */
    constructor(private userService: UserService) { }

    /**
     * Default overrided method.
     * @param request Request intercepted.
     * @param next Next request.
     */
    public intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        if (this.userService.loggedUser) {
            request = request.clone({
                setHeaders: {
                    Authorization: `Bearer ${ this.userService.loggedUser.token }`
                }
            });
        }

        return next.handle(request);
    }

}
