import { TestBed } from "@angular/core/testing";
import { ErrorInterceptor } from "src/app/common/interceptors/error.interceptor";
import { UserService } from "src/app/common/services/users.service";
import { instance, mock } from "ts-mockito";

let userServiceMock: UserService;

describe("ErrorInterceptor", () => {
    let service: ErrorInterceptor;

    beforeEach(() => {
        userServiceMock = mock(UserService);

        TestBed.configureTestingModule({
            providers: [
                ErrorInterceptor,
                { provide: UserService, useValue: instance(userServiceMock) },
            ]
        }).compileComponents();

        service = TestBed.get(ErrorInterceptor);
    });

    it("should create", async () => {
        expect(service).toBeTruthy();
    });

});
