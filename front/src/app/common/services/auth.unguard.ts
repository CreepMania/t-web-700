import { Injectable } from "@angular/core";
import { CanActivate, Router } from "@angular/router";
import { User } from "src/app/common/interfaces/iusers";
import { UserService } from "src/app/common/services/users.service";

/**
 * Service called to test unauth of client.
 */
@Injectable()

export class AuthUnguard implements CanActivate {

    /**
     * Contain the auth user.
     */
    public user: User = null;

    /**
     * Constructor of AuthUnguard.
     * @param userService service to get user state.
     * @param router router to renavigate.
     */
    constructor(private userService: UserService, private router: Router) {
        this.userService.getUserState().subscribe((u: User) => {
            this.user = u;
        });
    }

    /**
     * Method to override canActivate method of CanActivate class.
     */
    public canActivate(): boolean {
        if (this.user) {
            this.router.navigate(["/home"]);
            return false;
        }
        return true;
    }
}
