import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { isEmpty } from "lodash";
import { BehaviorSubject, Observable } from "rxjs";
import { User } from "src/app/common/interfaces/iusers";
import { BaseService } from "src/app/common/services/base.service";
import { ApiUrl } from "src/environments/iapiurl";

/**
 * Service to manage users.
 */
@Injectable()

export class UserService extends BaseService {

    /**
     * Local user stored (init to null).
     */
    public loggedUser: User = null;

    /**
     * Behavior Subject of User, if you subscribe to it, you will be up to date
     * for each userState.next method.
     */
    private userState: BehaviorSubject<User>;

    /**
     * Constructor of UserService.
     * @param http http client to execute request in BaseService.
     */
    constructor(http: HttpClient) {
        super(http, "users", ApiUrl.Users);
        this.userState = new BehaviorSubject<User>(this.loggedUser);
        this.userState.subscribe((u) => {
            this.loggedUser = u;
        });
    }

    /**
     * Method to call login path in api. (POST because password can contains "&")
     * @param login of user wan't to log in.
     * @param password of user wan't to log in.
     * @returns Returns an observable of string.
     */
    public loginUser(login: string, password: string): Observable<string> {
        return this.post<string>("login", {
            email: login,
            password: password
        });
    }

    /**
     * Method to call register path in api.
     * @param user user object to sign up.
     */
    public signupUser(user: User): Observable<string> {
        return this.post<string>("register", user);
    }

    /**
     * Method to call profile PUT path in api.
     * @param user user object to save.
     */
    public profileUser(user: User): Observable<string> {
        return this.put<string>("profile", user);
    }

    /**
     * Method to call profile GET path in api.
     * @returns User get.
     */
    public getProfile(): Observable<User> {
        return this.get<User>("profile");
    }

    /**
     * Method to logout an account.
     * @returns void.
     */
    public logoutUser(): Observable<any> {
        return this.post<any>("logout", null);
    }

    /**
     * Method to call auth/google path in api.
     */
    public googleAuth(): Observable<string> {
        return this.get<string>("auth/google");
    }

    /**
     * Init the sesion of user.
     * @returns void.
     */
    public async initSession() {
        const sessionTemp: User = this.getSession();
        if (!isEmpty(sessionTemp)) {
            this.loggedUser = sessionTemp;
            this.userState.next(this.loggedUser);
        }
    }

    /**
     * Getter for userState.
     * @returns Returns the BehaviorSubject of user.
     */
    public getUserState(): BehaviorSubject<User> {
        return this.userState;
    }

    /**
     * Get the session user in local sotrage if exist.
     * @returns The user session init from localStorage.
     */
    public getSession(): User {
        return JSON.parse(localStorage.getItem("CryptoWeb_Session"));
    }

    /**
     * Set the session user in local storage.
     * @param user to save in local storage.
     * @returns void.
     */
    public setSession(user: User): void {
        localStorage.setItem("CryptoWeb_Session", JSON.stringify(user));
        this.userState.next(user);
    }

    /**
     * Delete the session of user in local storage.
     * @returns void.
     */
    public deleteSession(): void {
        localStorage.removeItem("CryptoWeb_Session");
        this.userState.next(null);
    }

}
