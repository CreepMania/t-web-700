import { HttpClient } from "@angular/common/http";
import { TestBed } from "@angular/core/testing";
import { ArticlesService } from "src/app/common/services/articles.service";
import { instance, mock } from "ts-mockito";

let httpClientMock: HttpClient;

describe("ArticlesService", () => {
    let service: ArticlesService;

    beforeEach(() => {
        httpClientMock = mock(HttpClient);

        TestBed.configureTestingModule({
            providers: [
                ArticlesService,
                { provide: HttpClient, useValue: instance(httpClientMock) },
            ]
        }).compileComponents();

        service = TestBed.get(ArticlesService);
    });

    it("should create", async () => {
        expect(service).toBeTruthy();
    });

    it("should run #getAllArticles()", async () => {
        service.getAllArticles();
        expect().nothing();
    });

    it("should run #getAllArticles()", async () => {
        service.getAllArticles(["BTC", "ETH"]);
        expect().nothing();
    });
});
