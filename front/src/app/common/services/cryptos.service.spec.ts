import { HttpClient } from "@angular/common/http";
import { TestBed } from "@angular/core/testing";
import { Cryptocurrencies } from "src/app/common/interfaces/icryptocurrencies";
import { CryptosService } from "src/app/common/services/cryptos.service";
import { instance, mock } from "ts-mockito";

let httpClientMock: HttpClient;

describe("CryptosService", () => {
    let service: CryptosService;

    beforeEach(() => {
        httpClientMock = mock(HttpClient);

        TestBed.configureTestingModule({
            providers: [
                CryptosService,
                { provide: HttpClient, useValue: instance(httpClientMock) },
            ]
        }).compileComponents();

        service = TestBed.get(CryptosService);
    });

    it("should create", async () => {
        expect(service).toBeTruthy();
    });

    it("should run #getCryptos() without params", async () => {
        service.getCryptos([]);
        expect().nothing();
    });

    it("should run #getCryptos() with one params", async () => {
        service.getCryptos(["BTC"]);
        expect().nothing();
    });

    it("should run #getCryptos() with multiple params", async () => {
        service.getCryptos(["BTC", "ETH"]);
        expect().nothing();
    });

    it("should run #getHisto", async () => {
        service.getHisto("BTC", "30d", "USD");
        expect().nothing();
    });

    it("should run #createCryptos", async () => {
        service.createCryptos(new Cryptocurrencies({
            _id: "",
            enable: true,
            logo: "",
            name: "",
            shortName: ""
        }));
        expect().nothing();
    });

    it("should run #deleteCryptos", async () => {
        service.deleteCryptos(new Cryptocurrencies({
            _id: "",
            enable: true,
            logo: "",
            name: "",
            shortName: ""
        }));
        expect().nothing();
    });

    it("should run #updateCryptos", async () => {
        service.updateCryptos(new Cryptocurrencies({
            _id: "",
            enable: true,
            logo: "",
            name: "",
            shortName: ""
        }));
        expect().nothing();
    });

});
