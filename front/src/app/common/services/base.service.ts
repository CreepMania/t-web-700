import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { ApiUrl } from "src/environments/iapiurl";

import { environment } from "../../../environments/environment";

/**
 * BaseService extends on all services.
 */
export class BaseService {

    /**
     * Constructor to call (super()) on other services.
     * @param http http client to execute requests.
     * @param baseUrlService url of base service.
     * @param apiUrl multi api, so we need to know which use.
     */
    constructor(private http: HttpClient, private baseUrlService: string, private apiUrl: ApiUrl) { }

    /**
     * Method to get full path of an url.
     * @param url url to get full path.
     * @returns the full path url.
     */
    protected getURL(url: string): string {
        const envApi = environment.api_urls.filter((f) => {
            return f.key === this.apiUrl;
        })[0];
        const result = envApi.url + (envApi.port !== 80 ? ":" + envApi.port : "");
        return result + "/" + this.baseUrlService + "/" + url;
    }

    /**
     * Return on observable of http request with good url.
     * @param url url for get.
     * @returns an Observable of generic type T.
     */
    protected get<T>(url: string): Observable<T> {
        return this.http.get<T>(this.getURL(url));
    }

    /**
     * Return on observable of http request with good url.
     * @param url url for post.
     * @param params params for post body.
     * @returns an Observable of generic type T.
     */
    protected post<T>(url: string, params: any): Observable<T> {
        return this.http.post<T>(this.getURL(url), params);
    }

    /**
     * Return on observable of http request with good url.
     * @param url url for put.
     * @param params params for put body.
     * @returns an Observable of generic type T.
     */
    protected put<T>(url: string, params: any): Observable<T> {
        return this.http.put<T>(this.getURL(url), params);
    }

    /**
     * Return an observable of http request with good url.
     * @param url url for delete.
     */
    protected deleteType<T>(url: string): Observable<T> {
        return this.http.delete<T>(this.getURL(url));
    }

    /**
     * Return on observable of http request with good url.
     * @param url url for put.
     */
    protected delete(url: string): void {
        this.http.delete(this.getURL(url));
    }

}
