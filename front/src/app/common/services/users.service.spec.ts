import { HttpClient } from "@angular/common/http";
import { TestBed } from "@angular/core/testing";
import { Currency, User } from "src/app/common/interfaces/iusers";
import { UserService } from "src/app/common/services/users.service";
import { instance, mock } from "ts-mockito";

let httpClientMock: HttpClient;

describe("UserService", () => {
    let service: UserService;

    beforeEach(() => {
        httpClientMock = mock(HttpClient);

        let store = {};
        const mockLocalStorage = {
            getItem: (key: string): string => {
                return key in store ? store[key] : null;
            },
            setItem: (key: string, value: string) => {
                store[key] = `${ value }`;
            },
            removeItem: (key: string) => {
                delete store[key];
            },
            clear: () => {
                store = {};
            }
        };
        spyOn(localStorage, "getItem")
            .and.callFake(mockLocalStorage.getItem);
        spyOn(localStorage, "setItem")
            .and.callFake(mockLocalStorage.setItem);
        spyOn(localStorage, "removeItem")
            .and.callFake(mockLocalStorage.removeItem);
        spyOn(localStorage, "clear")
            .and.callFake(mockLocalStorage.clear);

        TestBed.configureTestingModule({
            providers: [
                UserService,
                { provide: HttpClient, useValue: instance(httpClientMock) },
            ]
        }).compileComponents();

        service = TestBed.get(UserService);
    });

    it("should create", async () => {
        expect(service).toBeTruthy();
    });

    it("should run #loginUser()", async () => {
        service.loginUser("test", "test");
        expect().nothing();
    });

    it("should run #signupUser()", async () => {
        service.signupUser(new User({
            _id: "",
            email: "test.test@epitech.eu",
            password: "",
            avatar: "",
            pseudo: "Test",
            admin: false,
            cryptocurrencies: [],
            defaultCurrency: Currency.EUR,
            filters: [],
            timestamp: "",
            token: ""
        }));
        expect().nothing();
    });

    it("should run #profileUser()", async () => {
        service.profileUser(new User({
            _id: "",
            email: "test.test@epitech.eu",
            password: "",
            avatar: "",
            pseudo: "Test",
            admin: false,
            cryptocurrencies: [],
            defaultCurrency: Currency.EUR,
            filters: [],
            timestamp: "",
            token: ""
        }));
        expect().nothing();
    });

    it("should run #profileUser()", async () => {
        service.getProfile();
        expect().nothing();
    });

    it("should run #logoutUser()", async () => {
        service.logoutUser();
        expect().nothing();
    });

    it("should run #initSession() without session", async () => {
        service.initSession();
        expect().nothing();
    });

    it("should run #deleteSession()", async () => {
        service.deleteSession();
        expect().nothing();
    });

    it("should run #initSession() with session", async () => {
        const user: User = new User({
            _id: "",
            email: "test.test@epitech.eu",
            password: "",
            avatar: "",
            pseudo: "Test",
            admin: false,
            cryptocurrencies: [],
            defaultCurrency: Currency.EUR,
            filters: [],
            timestamp: "",
            token: ""
        });
        localStorage.setItem("CryptoWeb_Session", JSON.stringify(user));
        service.initSession();
        expect().nothing();
    });

    it("should run #getUserState()", async () => {
        service.getUserState();
        expect().nothing();
    });

    it("should run #googleAuth()", async () => {
        service.googleAuth();
        expect().nothing();
    });

    it("should run #setSession()", async () => {
        const user: User = new User({
            _id: "",
            email: "test.test@epitech.eu",
            password: "",
            avatar: "",
            pseudo: "Test",
            admin: false,
            cryptocurrencies: [],
            defaultCurrency: Currency.EUR,
            filters: [],
            timestamp: "",
            token: ""
        });
        service.setSession(user);
        expect().nothing();
    });

});
