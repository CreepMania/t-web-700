import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { isEmpty } from "lodash";
import { BehaviorSubject, Observable } from "rxjs";
import { Articles } from "src/app/common/interfaces/iarticles";
import { BaseService } from "src/app/common/services/base.service";
import { ApiUrl } from "src/environments/iapiurl";

/**
 * Service to manage articles.
 */
@Injectable()

export class ArticlesService extends BaseService {

    /**
     * Constructor of ArticlesService.
     * @param http http client to execute request in BaseService.
     */
    constructor(http: HttpClient) {
        super(http, "articles", ApiUrl.Articles);
    }

    /**
     * Get all articles.
     */
    public getAllArticles(filterList: string[] = []): Observable<Articles[]> {
        if (filterList.length === 0) {
            return this.get("");
        }
        let url = "";
        filterList.forEach((element) => {
            url += element + ",";
        });
        url = url.substring(0, url.length - 1);
        return this.get("?filters=" + url);
    }
}
