import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { Cryptocurrencies, Cryptohisto } from "src/app/common/interfaces/icryptocurrencies";
import { BaseService } from "src/app/common/services/base.service";
import { ApiUrl } from "src/environments/iapiurl";

/**
 * Service to manage cryptos.
 */
@Injectable()

export class CryptosService extends BaseService {

    /**
     * Constructor of CryptosService.
     * @param http http client to execute request in BaseService.
     */
    constructor(http: HttpClient) {
        super(http, "cryptos", ApiUrl.Crypto);
    }

    /**
     * Method to call cryptos path in api.
     * @param params contains all needed cryptos (ex: BTC, ETH, ...).
     */
    public getCryptos(params: string[]): Observable<Cryptocurrencies[]> {
        if (params.length < 1) {
            return this.get("");
        }
        let urlParam = "?cmids=";
        for (let i = 0; i < params.length; i++) {
            const element = params[i];
            urlParam += element;
            if (i !== params.length - 1) {
                urlParam += ",";
            }
        }
        return this.get(urlParam);
    }

    /**
     * Create a new crypto currency.
     * @param param crypto to create.
     */
    public createCryptos(param: Cryptocurrencies): Observable<Cryptocurrencies> {
        return this.post("", param);
    }

    /**
     * Update the cryptocurrency passed to param.
     * @param param The cryptocurrency to update.
     */
    public updateCryptos(param: Cryptocurrencies): Observable<void> {
        return this.put(param._id, param);
    }

    /**
     * Delete the cryptocurrency.
     * @param param the cryptocurrency to delete.
     */
    public deleteCryptos(param: Cryptocurrencies): Observable<Boolean> {
        return this.deleteType<Boolean>(param._id);
    }

    /**
     * Method to call cryptos history path in api.
     * @param cmid ID of currency (ex: BTC, ETH, ...).
     * @param period number and string (ex: 365d, 23h, 59m, ...).
     * @param defaultcurrencies default currency (ex: EUR, USD).
     */
    public getHisto(cmid: string, period: string, defaultcurrencies: string): Observable<Cryptohisto> {
        return this.get(cmid + "/history/" + period + "?defaultcurrencies=" + defaultcurrencies);
    }

}
