/**
 * Cryptocurrencies class.
 */
export class Cryptocurrencies {

    /**
     * Id of cryptocurrencie.
     */
    public _id: string;

    /**
     * Name of cryptocurrencie.
     */
    public name: string;

    /**
     * Logo of cryptocurrencie.
     */
    public logo: string;

    /**
     * Short name of cryptocurrencie.
     */
    public shortName: string;

    /**
     * Enable or not cryptocurrencie.
     */
    public enable: boolean;

    /**
     * Eur value of cryptocurrencie.
     */
    public eur?: number;

    /**
     * Usd value of cryptocurrencie.
     */
    public usd?: number;

    /**
     * Constructor of Cryptocurrencie class.
     * @param obj Cryptocurrencies to create object.
     */
    constructor(obj?: Cryptocurrencies) {
        if (obj) {
            this._id = obj._id;
            this.name = obj.name;
            this.logo = obj.logo;
            this.shortName = obj.shortName;
            this.enable = obj.enable;
            this.eur = obj.eur;
            this.usd = obj.usd;
        }
    }

}

/**
 * Cryptohisto class.
 */
export class Cryptohisto {

    /**
     * Id of cryptohisto.
     */
    public _id: string;

    /**
     * Name of cryptohisto.
     */
    public name: string;

    /**
     * Logo of cryptohisto.
     */
    public logo: string;

    /**
     * Short name of cryptohisto.
     */
    public shortName: string;

    /**
     * Enabled of cryptohisto.
     */
    public enable: boolean;

    /**
     * Data of cryptohisto.
     */
    public data: Data;

    /**
     * Constructor of Cryptohisto class.
     * @param obj Cryptohisto to create object.
     */
    constructor(obj?: Cryptohisto) {
        if (obj) {
            this._id = obj._id;
            this.name = obj.name;
            this.logo = obj.logo;
            this.shortName = obj.shortName;
            this.enable = obj.enable;
            this.data = obj.data;
        }
    }
}

/**
 * Data class.
 */
export class Data {

    /**
     * Aggregated of data.
     */
    public Aggregated: boolean;

    /**
     * Time from of data.
     */
    public TimeFrom: number;

    /**
     * Time to of data.
     */
    public TimeTo: number;

    /**
     * Datas of data.
     */
    public Data?: DataEntity[];

    /**
     * Constructor of Data class.
     * @param obj Data to create object.
     */
    constructor(obj?: Data) {
        if (obj) {
            this.Aggregated = obj.Aggregated;
            this.TimeFrom = obj.TimeFrom;
            this.TimeTo = obj.TimeTo;
            this.Data = obj.Data;
        }
    }
}

/**
 * DataEntity class.
 */
export class DataEntity {

    /**
     * Time of data entity.
     */
    public time: number;

    /**
     * Higt value of data entity.
     */
    public high: number;

    /**
     * Low value of data entity.
     */
    public low: number;

    /**
     * Open of data entity.
     */
    public open: number;

    /**
     * Volume from of data entity.
     */
    public volumefrom: number;

    /**
     * Volume to of data entity.
     */
    public volumeto: number;

    /**
     * Close of data entity.
     */
    public close: number;

    /**
     * Conversion type of data entity.
     */
    public conversionType: string;

    /**
     * Conversion symbol of data entity.
     */
    public conversionSymbol: string;

    /**
     * Constructor of DataEntity class.
     * @param obj DataEntity to create object.
     */
    constructor(obj?: DataEntity) {
        if (obj) {
            this.time = obj.time;
            this.high = obj.high;
            this.low = obj.low;
            this.open = obj.open;
            this.volumefrom = obj.volumefrom;
            this.volumeto = obj.volumeto;
            this.close = obj.close;
            this.conversionType = obj.conversionType;
            this.conversionSymbol = obj.conversionSymbol;
        }
    }
}
