import { Cryptocurrencies } from "./icryptocurrencies";

/**
 * User class.
 */
export class User {

    /**
     * Id of user.
     */
    public _id: string;

    /**
     * Email of user.
     */
    public email: string;

    /**
     * Password of user.
     */
    public password: string;

    /**
     * Avatar of user.
     */
    public avatar: string;

    /**
     * Pseudo of user.
     */
    public pseudo: string;

    /**
     * Boolean to know if user is admin.
     */
    public admin: boolean;

    /**
     * Crypto-currencies of user.
     */
    public cryptocurrencies: Cryptocurrencies[];

    /**
     * Enum of default currency of user.
     */
    public defaultCurrency: Currency;

    /**
     * Filter of user (for articles).
     */
    public filters: string[];

    /**
     * Token of user (for OAuth).
     */
    public token: string;

    /**
     * Timestamp of token of user.
     */
    public timestamp: string;

    /**
     * Constructor of User class.
     * @param User to create class.
     */
    constructor(obj?: User) {
        if (obj) {
            this._id = obj._id;
            this.email = obj.email;
            this.password = obj.password;
            this.avatar = obj.avatar;
            this.pseudo = obj.pseudo;
            this.admin = obj.admin;
            this.cryptocurrencies = [];
            if (obj.cryptocurrencies && obj.cryptocurrencies.length > 0) {
                const self: this = this;
                obj.cryptocurrencies.forEach((element) => {
                    self.cryptocurrencies.push(new Cryptocurrencies(element));
                });
            }
            this.defaultCurrency = obj.defaultCurrency;
            this.filters = [];
            if (obj.filters && obj.filters.length > 0) {
                const self: this = this;
                obj.filters.forEach((element) => {
                    self.filters.push(element);
                });
            }
            this.token = obj.token;
            this.timestamp = obj.timestamp;
        }
    }

}

/**
 * Enum of enabled currency.
 */
export enum Currency {

    EUR = 1,
    USD = 2

}
