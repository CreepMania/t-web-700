/**
 * Articles class
 */
export class Articles {
    /**
     * Id of article.
     */
    public id: string;
    /**
     * GUID of article.
     */
    public guid: string;
    /**
     * Timestamp of article creation.
     */
    public published_on: number;

    /**
     * Conversion of the timestamp to date.
     */
    public published_on_date: Date;

    /**
     * True if the article was made today.
     */
    public published_today: number;
    /**
     * URL of image.
     */
    public imageurl: string;
    /**
     * Title of article.
     */
    public title: string;
    /**
     * URL of article.
     */
    public url: string;
    /**
     * Source of article.
     */
    public source: string;
    /**
     * Body of article.
     */
    public body: string;
    /**
     * Tags of article.
     */
    public tags: string;
    /**
     * Category of article.
     */
    public categories: string;
    /**
     * Upvote of article.
     */
    public upvotes: string;
    /**
     * Downvote of article.
     */
    public downvotes: string;
    /**
     * Langage of article.
     */
    public lang: string;
    /**
     * Info of the source of article.
     */
    public source_info: SourceInfo;

    /**
     * Constructor for Article class.
     * @param obj obj to create.
     */
    constructor(obj?: any) {
    }
}

/**
 * Interface for SourceInfo.
 */
export interface SourceInfo {
    /**
     * Name of source.
     */
    name: string;
    /**
     * Langage of source.
     */
    lang: string;
    /**
     * Image of source.
     */
    img: string;
}
