export enum Period {
    Minute = "m",
    Hour = "h",
    Day = "d"
}
