import { GuidedTour, GuidedTourService, Orientation } from "ngx-guided-tour";

/**
 * Helper component to show helper view
 */
export abstract class HelperComponent {

    /**
     * helpTours contains all guided tours.
     */
    public helpTours: { [id: string]: GuidedTour } = {
        "header-tour": {
            tourId: "header-tour",
            steps: [
                {
                    title: "Header help tour !",
                    selector: ".tour-header-brand",
                    content: "Redirect to home page.",
                    orientation: Orientation.Bottom
                },
                {
                    title: "Home button",
                    selector: ".tour-header-home",
                    content: "Home page contains news.",
                    orientation: Orientation.Bottom
                },
                {
                    title: "Cryptos button",
                    selector: ".tour-header-cryptos",
                    content: "Cryptos page contains history of cryptos.",
                    orientation: Orientation.Bottom
                },
                {
                    title: "Login button",
                    selector: ".tour-header-login",
                    content: "Login page is for login to application with login and password.",
                    orientation: Orientation.Bottom
                },
                {
                    title: "Sign up button",
                    selector: ".tour-header-signup",
                    content: "Signup on application with some informations.",
                    orientation: Orientation.Bottom
                },
                {
                    title: "Admin button",
                    selector: ".tour-header-admin",
                    content: "Manage cryptos, currencies and anonymous accounts.",
                    orientation: Orientation.Bottom
                },
                {
                    title: "Profile button",
                    selector: ".tour-header-profile",
                    content: "Display and change your account informations.",
                    orientation: Orientation.Bottom
                },
                {
                    title: "Logout button",
                    selector: ".tour-header-logout",
                    content: "Log out of application, you will be anonymous.",
                    orientation: Orientation.Bottom
                }
            ]
        },
        "home-tour": {
            tourId: "home-tour",
            steps: [
                {
                    title: "Home help tour !",
                    selector: ".tour-home-container",
                    content: "Container for article to show some informations.",
                    orientation: Orientation.Right
                },
                {
                    title: "Image of article",
                    selector: ".tour-home-image",
                    content: "The image of source article.",
                    orientation: Orientation.Right
                },
                {
                    title: "Title of article",
                    selector: ".tour-home-title",
                    content: "The title of source article.",
                    orientation: Orientation.Right
                },
                {
                    title: "Link of article",
                    selector: ".tour-home-link",
                    content: "The source link of article.",
                    orientation: Orientation.Right
                },
                {
                    title: "Up votes of article",
                    selector: ".tour-home-upvotes",
                    content: "The up votes values of source article.",
                    orientation: Orientation.Right
                },
                {
                    title: "Down votes of article",
                    selector: ".tour-home-downvotes",
                    content: "The down votes values of source article.",
                    orientation: Orientation.Right
                },
                {
                    title: "Publish date of article",
                    selector: ".tour-home-date",
                    content: "The published date or hour if this day of source article.",
                    orientation: Orientation.Right
                },
                {
                    title: "Activate filter of profile",
                    selector: ".tour-home-filters",
                    content: "Activate or not profile filter usage.",
                    orientation: Orientation.Bottom
                }
            ]
        },
        "cryptos-tour": {
            tourId: "cryptos-tour",
            steps: [
                {
                    title: "Cryptos help tour !",
                    selector: ".tour-cryptos-period-value",
                    content: "Give a value for period.",
                    orientation: Orientation.Bottom
                },
                {
                    title: "Period criteria",
                    selector: ".tour-cryptos-period",
                    content: "Choose a period : minutes, hours or days.",
                    orientation: Orientation.Bottom
                },
                {
                    title: "Apply all criterias",
                    selector: ".tour-cryptos-apply",
                    content: "Apply your criteria to all cryptos charts.",
                    orientation: Orientation.Bottom
                },
                {
                    title: "Container for a chart",
                    selector: ".tour-cryptos-cryptos",
                    content: "This container provide you some informations for cryptos history.",
                    orientation: Orientation.Top
                },
                {
                    title: "Logo of crypto",
                    selector: ".tour-cryptos-logo",
                    content: "The logo of crypto-money.",
                    orientation: Orientation.Bottom
                },
                {
                    title: "The value in EUR",
                    selector: ".tour-cryptos-eur",
                    content: "Provide last value of crypto-money in EUR.",
                    orientation: Orientation.Bottom
                },
                {
                    title: "The value in USD",
                    selector: ".tour-cryptos-usd",
                    content: "Provide last value of crypto-money in USD.",
                    orientation: Orientation.Bottom
                },
                {
                    title: "History chart of crypto",
                    selector: ".tour-cryptos-chart",
                    content: "Provide all values of crypto-money in choosed default currency or EUR by default. You can show or hide a values chain.",
                    orientation: Orientation.Top
                }
            ]
        },
        "login-tour": {
            tourId: "login-tour",
            steps: [
                {
                    title: "Login help tour !",
                    selector: ".tour-login-login",
                    content: "Login input to connect on application, provide your email.",
                    orientation: Orientation.Bottom
                },
                {
                    title: "Password field",
                    selector: ".tour-login-password",
                    content: "Password input to connect on application, type your password.",
                    orientation: Orientation.Bottom
                },
                {
                    title: "Login button",
                    selector: ".tour-login-login-button",
                    content: "Button to login.",
                    orientation: Orientation.Top
                },
                {
                    title: "Signup button",
                    selector: ".tour-login-signup-button",
                    content: "Button to redirect to sign up page.",
                    orientation: Orientation.Top
                },
                {
                    title: "Google button",
                    selector: ".tour-login-google",
                    content: "Button to continue with using google, it will log you in and get your google email and avatar.",
                    orientation: Orientation.Top
                }
            ]
        },
        "signup-tour": {
            tourId: "signup-tour",
            steps: [
                {
                    title: "Sign up help tour !",
                    selector: ".tour-signup-pseudo",
                    content: "Pseudo input to choose your account pseudo's.",
                    orientation: Orientation.Bottom
                },
                {
                    title: "Email field",
                    selector: ".tour-signup-email",
                    content: "Email input to choose your account email's.",
                    orientation: Orientation.Bottom
                },
                {
                    title: "Password field",
                    selector: ".tour-signup-password",
                    content: "Password input to choose your account password's.",
                    orientation: Orientation.Bottom
                },
                {
                    title: "Password repeat field",
                    selector: ".tour-signup-password-repeat",
                    content: "Password repeat input, value might be same as password field.",
                    orientation: Orientation.Bottom
                },
                {
                    title: "Signup button",
                    selector: ".tour-signup-signup-button",
                    content: "Button to sign up.",
                    orientation: Orientation.Top
                },
                {
                    title: "Login button",
                    selector: ".tour-signup-login-button",
                    content: "Button to redirect to login page.",
                    orientation: Orientation.Top
                }
            ]
        },
        "admin-tour": {
            tourId: "admin-tour",
            steps: [
                {
                    title: "Admin help tour !",
                    selector: ".tour-admin-cryptos-table",
                    content: "Table of all cryptos registred in web site.",
                    orientation: Orientation.Top
                },
                {
                    title: "Name field",
                    selector: ".tour-admin-crypto-name-field",
                    content: "Define the name of your crypto (will be displayed in 'Cryptos' page).",
                    orientation: Orientation.Top
                },
                {
                    title: "Short name field",
                    selector: ".tour-admin-crypto-shortname-field",
                    content: "Define the short name of your crypto (will be used to get crypto history, be sure it exist).",
                    orientation: Orientation.Top
                },
                {
                    title: "Logo field",
                    selector: ".tour-admin-crypto-logo-field",
                    content: "Define the logo of your crypto (it MUST be an url to image only).",
                    orientation: Orientation.Top
                },
                {
                    title: "Enable checkbox",
                    selector: ".tour-admin-crypto-enable-field",
                    content: "Define if crypto is enable or not for common user, if you disable one, only admin can saw her.",
                    orientation: Orientation.Top
                },
                {
                    title: "Save button",
                    selector: ".tour-admin-crypto-save-button",
                    content: "Save the crypto line changement, if it's a new crypto, will create it.",
                    orientation: Orientation.Top
                },
                {
                    title: "Delete button",
                    selector: ".tour-admin-crypto-delete-button",
                    content: "Delete the crypto currency.",
                    orientation: Orientation.Top
                },
                {
                    title: "Cancel button",
                    selector: ".tour-admin-crypto-cancel-button",
                    content: "Cancel the crypto currency creation.",
                    orientation: Orientation.Top
                },
                {
                    title: "Add button",
                    selector: ".tour-admin-crypto-add-button",
                    content: "Add a new line on table.",
                    orientation: Orientation.Top
                }
            ]
        },
        "profile-tour": {
            tourId: "profile-tour",
            steps: [
                {
                    title: "Profile help tour !",
                    selector: ".tour-profile-avatar",
                    content: "Profile avatar field, you can upload only .png file.",
                    orientation: Orientation.Bottom
                },
                {
                    title: "Pseudo field",
                    selector: ".tour-profile-pseudo",
                    content: "Pseudo input to edit your account pseudo's.",
                    orientation: Orientation.Bottom
                },
                {
                    title: "Email field",
                    selector: ".tour-profile-email",
                    content: "Email input to edit your account email's.",
                    orientation: Orientation.Bottom
                },
                {
                    title: "Default currency combo-box",
                    selector: ".tour-profile-default-currency",
                    content: "Default currency combo-box to edit your default currency used in home page.",
                    orientation: Orientation.Bottom
                },
                {
                    title: "Filters for news",
                    selector: ".tour-profile-filters",
                    content: "Manage your account filters.",
                    orientation: Orientation.Bottom
                },
                {
                    title: "Filter value",
                    selector: ".tour-profile-filter-field",
                    content: "Edit a filter field.",
                    orientation: Orientation.Bottom
                },
                {
                    title: "Remove a filter",
                    selector: ".tour-profile-delete",
                    content: "Remove an existing filter.",
                    orientation: Orientation.Top
                },
                {
                    title: "Add a new filter",
                    selector: ".tour-profile-add",
                    content: "Add a new filter to your filters list.",
                    orientation: Orientation.Top
                },
                {
                    title: "Update your profile",
                    selector: ".tour-profile-update",
                    content: "Post your modifications of account profile.",
                    orientation: Orientation.Top
                }
            ]
        }
    };

    /**
     * Constructor of HelperComponent.
     * @param guidedTourService used to start tour.
     */
    constructor(private guidedTourService: GuidedTourService) { }

    /**
     * Show a guided tour for a specific component.
     * @param id id of guided tour to show.
     */
    public showHelpTour(id: string) {
        this.guidedTourService.startTour(this.helpTours[id]);
    }

}
