import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AppCommonModule } from "src/app/common/common.module";
import { AdminGuard } from "src/app/common/services/admin.guard";
import { AuthGuard } from "src/app/common/services/auth.guard";
import { AuthUnguard } from "src/app/common/services/auth.unguard";
import { AdminComponent } from "src/app/components/admin/admin.component";
import { CryptosComponent } from "src/app/components/cyptos/cryptos.component";
import { HomeComponent } from "src/app/components/home/home.component";
import { LoginComponent } from "src/app/components/login/login.component";
import { NotfoundComponent } from "src/app/components/notfound/notfound.component";
import { ProfileComponent } from "src/app/components/profile/profile.component";
import { SignupComponent } from "src/app/components/signup/signup.component";

const routes: Routes = [
    { path: "", pathMatch: "full", redirectTo: "home" },
    { path: "home", component: HomeComponent },
    { path: "login", component: LoginComponent, canActivate: [AuthUnguard] },
    { path: "signup", component: SignupComponent, canActivate: [AuthUnguard] },
    { path: "profile", component: ProfileComponent, canActivate: [AuthGuard] },
    { path: "cryptos", component: CryptosComponent },
    { path: "admin", component: AdminComponent, canActivate: [AdminGuard] },
    { path: "**", component: NotfoundComponent }
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes),
        AppCommonModule
    ],
    exports: [
        RouterModule
    ]
})

export class AppRoutingModule { }
