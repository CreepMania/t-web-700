import { TestBed } from "@angular/core/testing";
import { RouterTestingModule } from "@angular/router/testing";
import { AppComponent } from "src/app/app.component";
import { AppCommonModule } from "src/app/common/common.module";
import { UserService } from "src/app/common/services/users.service";
import { instance, mock } from "ts-mockito";

let userServiceMock: UserService;
describe("AppComponent", () => {
    let component: AppComponent;

    beforeEach(() => {
        userServiceMock = mock(UserService);

        TestBed.configureTestingModule({
            providers: [
                AppComponent,
                { provide: UserService, useValue: instance(userServiceMock) }
            ],
            imports: [
                RouterTestingModule,
                AppCommonModule
            ]
        }).compileComponents();

        component = TestBed.get(AppComponent);
    });

    it("should create", async () => {
        expect(component).toBeTruthy();
    });

});
