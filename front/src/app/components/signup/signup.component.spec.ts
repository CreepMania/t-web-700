import { TestBed } from "@angular/core/testing";
import { FormsModule } from "@angular/forms";
import { Router } from "@angular/router";
import { GuidedTourService } from "ngx-guided-tour";
import { ToastrModule, ToastrService } from "ngx-toastr";
import { of, throwError } from "rxjs";
import { Cryptocurrencies } from "src/app/common/interfaces/icryptocurrencies";
import { Currency, User } from "src/app/common/interfaces/iusers";
import { UserService } from "src/app/common/services/users.service";
import { SignupComponent } from "src/app/components/signup/signup.component";
import { instance, mock } from "ts-mockito";

let userServiceMock: UserService;
let routerServiceMock: Router;
let toastrServiceMock: ToastrService;

describe("SignupComponent", () => {
    let userService: UserService;
    let routerService: Router;
    let toastrService: ToastrService;
    let component: SignupComponent;

    beforeEach(() => {
        userServiceMock = mock(UserService);
        routerServiceMock = mock(Router);
        toastrServiceMock = mock(ToastrService);

        TestBed.configureTestingModule({
            providers: [
                SignupComponent,
                { provide: UserService, useValue: instance(userServiceMock) },
                { provide: Router, useValue: instance(routerServiceMock) },
                { provide: ToastrService, useValue: instance(toastrServiceMock) },
                { provide: GuidedTourService }
            ],
            imports: [
                FormsModule,
                ToastrModule
            ]
        }).compileComponents();

        component = TestBed.get(SignupComponent);
        userService = TestBed.get(UserService);
        routerService = TestBed.get(Router);
        toastrService = TestBed.get(ToastrService);
    });

    it("should create", async () => {
        expect(component).toBeTruthy();
    });

    it("should run #doSignup() already on signing up", async () => {
        const event = new MouseEvent("submit");
        component.onSignup = true;
        component.doSignup(event);
        expect(component.error === "").toBeTruthy();
    });

    it("should run #doSignup() empty fields", async () => {
        const event = new MouseEvent("submit");
        component.doSignup(event);
        expect(component.error === "Please provide a value for all fields.").toBeTruthy();
    });

    it("should run #doSignup() different password", async () => {
        const event = new MouseEvent("submit");
        component.pseudo = "Test";
        component.email = "test.test@epitech.eu";
        component.password = "test";
        component.passwordRepeat = "test2";
        component.doSignup(event);
        expect(component.error === "Password and repeat should be same.").toBeTruthy();
    });

    it("should run #doSignup() sign up fail", async () => {
        const event = new MouseEvent("submit");
        component.pseudo = "Test";
        component.email = "test.test@epitech.eu";
        component.password = "test";
        component.passwordRepeat = "test";
        spyOn(userService, "signupUser").and.returnValue(throwError({ status: 404 }));
        component.doSignup(event);
        expect(component.error === "Signup: An error occure during signup.").toBeTruthy();
    });

    it("should run #doSignup() sign up done", async () => {
        const event = new MouseEvent("submit");
        component.pseudo = "Test";
        component.email = "test.test@epitech.eu";
        component.password = "test";
        component.passwordRepeat = "test";
        spyOn(userService, "signupUser").and.returnValue(of("Token"));
        spyOn(userService, "getProfile").and.returnValue(of(new User({
            _id: "",
            email: "test.test@epitech.eu",
            password: "",
            avatar: "",
            pseudo: "Test",
            admin: false,
            cryptocurrencies: [
                new Cryptocurrencies(null)
            ],
            defaultCurrency: Currency.EUR,
            filters: [
                "test"
            ],
            timestamp: "",
            token: ""
        })));
        component.doSignup(event);
        expect(component.onSignup === false).toBeTruthy();
    });

    it("should run #redirect()", async () => {
        const event = new MouseEvent("submit");
        component.redirect(event, "/login");
        expect().nothing();
    });
});
