import { CommonModule, DatePipe } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { ChartsModule } from "ng2-charts";
import { AdminComponent } from "src/app/components/admin/admin.component";
import { CryptosComponent } from "src/app/components/cyptos/cryptos.component";
import { HomeComponent } from "src/app/components/home/home.component";
import { LoginComponent } from "src/app/components/login/login.component";
import { NotfoundComponent } from "src/app/components/notfound/notfound.component";
import { ProfileComponent } from "src/app/components/profile/profile.component";
import { SignupComponent } from "src/app/components/signup/signup.component";

@NgModule({
    declarations: [
        LoginComponent,
        HomeComponent,
        SignupComponent,
        ProfileComponent,
        CryptosComponent,
        AdminComponent,
        NotfoundComponent
    ],
    imports: [
        CommonModule,
        FormsModule,
        ChartsModule
    ],
    providers: [
        DatePipe
    ]
})

export class ComponentsModule { }
