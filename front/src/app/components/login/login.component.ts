import { Component } from "@angular/core";
import { Router } from "@angular/router";
import { GuidedTourService } from "ngx-guided-tour";
import { ToastrService } from "ngx-toastr";
import * as sha256 from "sha256";
import { HelperComponent } from "src/app/common/helper/helper.component";
import { User } from "src/app/common/interfaces/iusers";
import { UserService } from "src/app/common/services/users.service";

/**
 * Component to log in on application.
 */
@Component({
    selector: "app-login",
    templateUrl: "./login.component.html",
    styleUrls: ["./login.component.scss"]
})

export class LoginComponent extends HelperComponent {

    /**
     * Contains login field value.
     */
    public login = "";

    /**
     * Contains password field value.
     */
    public password = "";

    /**
     * Contains error message.
     */
    public error = "";

    /**
     * Boolean to know if request is on pending.
     */
    public onConnection = false;

    /**
     * Constructor of LoginComponent.
     * @param userService User service to handle login method.
     * @param toastr Toasts service to show toasts.
     * @param router Router service to redirect after login.
     */
    constructor(private userService: UserService, private toastr: ToastrService, private router: Router, guidedTourService: GuidedTourService) {
        super(guidedTourService);
    }

    /**
     * Method handled to submit login form.
     * @param e Event of submited form (used to prevent default reload action)
     */
    public doLogin(e: MouseEvent): void {
        if (this.onConnection) {
            return;
        }
        if (!this.login || !this.password) {
            this.toastr.warning("Please provide a value for all fields.", "Connection");
            this.error = "Please provide a value for all fields.";
            return;
        }
        this.onConnection = true;
        this.error = "";
        this.toastr.info("Connection in progress, please wait ...", "Connection");
        e.preventDefault();
        this.userService.loginUser(this.login, sha256(this.password)).subscribe((t: string) => {
            this.getProfileByToken(t);
        }, (err) => {
            this.toastr.clear();
            this.toastr.error("An error occure during connection.", "Connection");
            this.error = "Connection: An error occure during connection.";
            this.onConnection = false;
        });
    }

    /**
     * Method handled for sign in redirect.
     * @param e event to prevent form.
     * @param url url to redirect.
     */
    public redirect(e: any, url: string) {
        e.preventDefault();
        this.router.navigateByUrl(url);
    }

    /**
     * Method called when user click on "Continue with Google" button.
     */
    public doLoginGoogle() {
        this.userService.googleAuth().subscribe((s) => {
            this.doAuthorization(s);
        });
    }

    /**
     * Method to get authorization by google.
     * @param url to check authorization (by generating a popup).
     */
    public doAuthorization(url: string) {
        let loopCount = 60;
        const windowHandle: Window = this.createOauthWindow(url, "OAuth login");
        const self: this = this;
        if (this.onConnection) {
            return;
        }
        this.onConnection = true;
        this.error = "";
        this.toastr.info("Connection in progress, please wait ...", "Connection");
        const intervalId = window.setInterval(() => {
            if (loopCount-- < 0) {
                window.clearInterval(intervalId);
                windowHandle.close();
            } else {
                let href: string;
                try {
                    href = windowHandle.location.href;
                } catch (e) {
                    console.log("Error : ", e);
                }

                if (href && href !== "about:blank") {
                    window.clearInterval(intervalId);
                    windowHandle.close();
                    const token = windowHandle.document.documentElement.innerText.substring(1, windowHandle.document.documentElement.innerText.length - 1);
                    this.getProfileByToken(token);
                }
            }
        }, 1000);
    }

    /**
     * Method to create an oAuth window (for google atm).
     * @param url of window.
     * @param name of window.
     * @param width of window.
     * @param height of window.
     * @param left of window.
     * @param top of window.
     */
    public createOauthWindow(url: string, name = "Authorization", width = 500, height = 600, left = 0, top = 0) {
        if (url == null) {
            return null;
        }
        const options = `width=${ width },height=${ height },left=${ left },top=${ top }`;
        return window.open(url, name, options);
    }

    /**
     * Method to get profile by a token.
     * @param token token to auth.
     */
    private getProfileByToken(token: string) {
        this.userService.loggedUser = new User();
        this.userService.loggedUser.token = token;
        this.userService.getProfile().subscribe((u: User) => {
            u.token = token;
            this.toastr.clear();
            this.toastr.success("You correctly logged in.", "Connection");
            this.onConnection = false;
            this.userService.setSession(u);
            this.router.navigateByUrl("home");
        });
    }

}
