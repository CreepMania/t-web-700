import { TestBed } from "@angular/core/testing";
import { FormsModule } from "@angular/forms";
import { Router } from "@angular/router";
import { GuidedTourService } from "ngx-guided-tour";
import { ToastrModule, ToastrService } from "ngx-toastr";
import { of, throwError } from "rxjs";
import { Cryptocurrencies } from "src/app/common/interfaces/icryptocurrencies";
import { Currency, User } from "src/app/common/interfaces/iusers";
import { UserService } from "src/app/common/services/users.service";
import { LoginComponent } from "src/app/components/login/login.component";
import { instance, mock } from "ts-mockito";

let userServiceMock: UserService;
let routerServiceMock: Router;
let toastrServiceMock: ToastrService;

describe("LoginComponent", () => {

    let userService: UserService;
    let component: LoginComponent;

    beforeEach(() => {
        userServiceMock = mock(UserService);
        routerServiceMock = mock(Router);
        toastrServiceMock = mock(ToastrService);
        TestBed.configureTestingModule({
            providers: [
                LoginComponent,
                { provide: UserService, useValue: instance(userServiceMock) },
                { provide: Router, useValue: instance(routerServiceMock) },
                { provide: ToastrService, useValue: instance(toastrServiceMock) },
                { provide: GuidedTourService }
            ],
            imports: [
                FormsModule,
                ToastrModule
            ]
        }).compileComponents();

        component = TestBed.get(LoginComponent);
        userService = TestBed.get(UserService);
    });

    it("should create", async () => {
        expect(component).toBeTruthy();
    });

    it("should run #doLogin() already on connection", async () => {
        const event = new MouseEvent("submit");
        const user = new User(null);
        component.onConnection = true;
        component.doLogin(event);
        expect(component.error === "").toBeTruthy();
    });

    it("should run #doLogin() empty fields", async () => {
        const event = new MouseEvent("submit");
        component.doLogin(event);
        expect(component.error === "Please provide a value for all fields.").toBeTruthy();
    });

    it("should run #doLogin() connection fail", async () => {
        const event = new MouseEvent("submit");
        component.login = "test";
        component.password = "test";
        spyOn(userService, "loginUser").and.returnValue(throwError({ status: 404 }));
        component.doLogin(event);
        expect(component.error === "Connection: An error occure during connection.").toBeTruthy();
    });

    it("should run #doLogin() connection done", async () => {
        const event = new MouseEvent("submit");
        component.login = "test";
        component.password = "test";
        spyOn(userService, "loginUser").and.returnValue(of("Token"));
        spyOn(userService, "getProfile").and.returnValue(of(new User({
            _id: "",
            email: "test.test@epitech.eu",
            password: "",
            avatar: "",
            pseudo: "Test",
            admin: false,
            cryptocurrencies: [
                new Cryptocurrencies(null)
            ],
            defaultCurrency: Currency.EUR,
            filters: [],
            timestamp: "",
            token: ""
        })));
        component.doLogin(event);
        expect(component.onConnection === false).toBeTruthy();
    });

    it("should run #redirect()", async () => {
        const event = new MouseEvent("submit");
        component.redirect(event, "/signin");
        expect().nothing();
    });

    it("should run #doLoginGoogle() with connection", async () => {
        component.onConnection = true;
        spyOn(userService, "googleAuth").and.returnValue(of("Token"));
        component.doLoginGoogle();
        expect().nothing();
    });

    it("should run #doLoginGoogle() without connection", async () => {
        component.onConnection = false;
        spyOn(userService, "googleAuth").and.returnValue(of("Token"));
        component.doLoginGoogle();
        expect().nothing();
    });

    it("should run #createOauthWindow() cover branches", async () => {
        component.createOauthWindow(null, "Test");
        expect().nothing();
    });

});
