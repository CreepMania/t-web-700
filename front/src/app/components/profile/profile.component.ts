import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import { cloneDeep, isEqual } from "lodash";
import { GuidedTourService } from "ngx-guided-tour";
import { ToastrService } from "ngx-toastr";
import { HelperComponent } from "src/app/common/helper/helper.component";
import { Currency, User } from "src/app/common/interfaces/iusers";
import { UserService } from "src/app/common/services/users.service";

/**
 * Component to display and manage logged user profile.
 */
@Component({
    selector: "app-profile",
    templateUrl: "./profile.component.html",
    styleUrls: ["./profile.component.scss"]
})

export class ProfileComponent extends HelperComponent implements OnInit {

    /**
     * Contains user of profile.
     */
    public user: User = null;

    /**
     * Contains the user saved (to compare with "isEqual" on save).
     */
    public userSaved: User = null;

    /**
     * Contains form error.
     */
    public error = "";

    /**
     * Boolean to know if request is on pending.
     */
    public onUpdate = false;

    /**
     * Boolean to know if page is load or not.
     */
    public isLoad = false;

    /**
     * Contains the html input type "file" for avatar.
     */
    @ViewChild("fileInput", { static: false }) private fileInput: ElementRef<HTMLInputElement>;

    /**
     * Constructor of ProfileComponent.
     * @param userService Service of users.
     * @param toastr Toasts service to show toasts.
     */
    constructor(private userService: UserService, private toastr: ToastrService, guidedTourService: GuidedTourService) {
        super(guidedTourService);
    }

    /**
     * Method call when component is on initialisation.
     */
    public ngOnInit() {
        this.userService.getUserState().subscribe((u: User) => {
            this.user = u;
            this.userSaved = cloneDeep(this.user);
            this.isLoad = true;
        });
    }

    /**
     * Method call when user push on "Update" button.
     * @param e Mouse event to prevent default action.
     */
    public doUpdate(e: MouseEvent): void {
        if (this.onUpdate) {
            return;
        }

        // Manage empty filter(s)
        const filteredFilters = [];
        for (let i = 0; i < this.user.filters.length; i++) {
            const element = this.user.filters[i];
            if (element) {
                filteredFilters.push(element);
            }
        }
        this.user.filters = filteredFilters;

        // Check if profile got modified
        if (isEqual(this.user, this.userSaved)) {
            this.toastr.warning("No data changed", "Update profile");
            this.error = "No data changed.";
            return;
        }

        // Check profile fields
        if (!this.user.pseudo || !this.user.email) {
            this.toastr.warning("Please provide a value for all fields.", "Update profile");
            this.error = "Please provide a value for all fields.";
            return;
        }

        this.onUpdate = true;
        this.error = "";
        this.toastr.info("Update in progress, please wait ...", "Update profile");
        e.preventDefault();
        this.userService.profileUser(this.user).subscribe((s: string) => {
            this.userSaved = cloneDeep(this.user);
            this.userService.setSession(this.user);
            this.toastr.clear();
            this.toastr.success("You correctly update your profile.", "Update profile");
            this.onUpdate = false;
        }, (err) => {
            this.toastr.clear();
            this.toastr.error("An error occure during update.", "Update profile");
            this.error = "Update profile: An error occure during profile.";
            this.onUpdate = false;
        });
    }

    /**
     * Get all keys of Currency enum for combobox.
     */
    public getCurrenciesKeys(): any[] {
        const result = [];
        Object.keys(Currency).filter((key) => typeof Currency[key as any] === "number").forEach((key) => {
            result.push({
                key: Currency[key],
                value: key
            });
        });
        return result;
    }

    /**
     * Method handled on button "Add filter" click.
     * @param event mouse event to prevent default form post.
     */
    public addFilter(event: MouseEvent): void {
        event.preventDefault();
        this.user.filters.push("");
    }

    /**
     * Method handled on button "Remove" click.
     * @param event mouse event to prevent default form post.
     * @param index index of filter in list.
     */
    public deleteFilter(event: MouseEvent, index: number): void {
        event.preventDefault();
        this.user.filters.splice(index, 1);
    }

    /**
     * Trackby method to prevent DOM redraw.
     * @param index index of filter.
     * @param obj obj of filter.
     */
    public trackByIndex(index: number, obj: any): any {
        return index;
    }

    /**
     * Method called only for open the file chooser.
     */
    public openChangeAvatar(): any {
        this.fileInput.nativeElement.click();
    }

    /**
     * Method handled after file input fill.
     * @param event event to take file selected.
     */
    public changeAvatar(event: any): any {
        this.onUpdate = true;
        this.error = "";
        this.toastr.info("Image upload in progress ...", "Upload avatar");

        // Get file selected (method is called only when one file is selected)
        const file: File = event.target.files[0];

        // Check image type (only accept PNG)
        if (file.type !== "image/png") {
            this.error = "Upload avatar: Only png file type supported.";
            this.toastr.error("Only png file type supported.", "Upload avatar");
            return;
        }

        // Load blob and reader
        const blob = file as Blob;
        const reader = new FileReader();
        reader.readAsDataURL(blob);
        const self = this;

        // Call it when image is load
        reader.onloadend = function () {
            const base64data = reader.result;
            self.user.avatar = base64data.toString();
            self.onUpdate = false;
            self.error = "";
            self.toastr.success("Image upload is well done.", "Upload avatar");
        };

        // Call it when image loading failed
        reader.onerror = function () {
            self.onUpdate = false;
            self.error = "Upload avatar: An error occured while trying to upload your avatar.";
            self.toastr.error("Image upload is well done.", "Upload avatar");
        };
    }

}
