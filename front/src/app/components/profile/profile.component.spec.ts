import { TestBed } from "@angular/core/testing";
import { FormsModule } from "@angular/forms";
import { isEqual } from "lodash";
import { GuidedTourService } from "ngx-guided-tour";
import { ToastrModule, ToastrService } from "ngx-toastr";
import { of, throwError } from "rxjs";
import { Cryptocurrencies } from "src/app/common/interfaces/icryptocurrencies";
import { Currency, User } from "src/app/common/interfaces/iusers";
import { UserService } from "src/app/common/services/users.service";
import { ProfileComponent } from "src/app/components/profile/profile.component";
import { instance, mock } from "ts-mockito";

let userServiceMock: UserService;
let toastrServiceMock: ToastrService;

describe("ProfileComponent", () => {
    let userService: UserService;
    let component: ProfileComponent;

    beforeEach(() => {
        userServiceMock = mock(UserService);
        toastrServiceMock = mock(ToastrService);

        TestBed.configureTestingModule({
            providers: [
                ProfileComponent,
                { provide: UserService, useValue: instance(userServiceMock) },
                { provide: ToastrService, useValue: instance(toastrServiceMock) },
                { provide: GuidedTourService }
            ],
            imports: [
                FormsModule,
                ToastrModule
            ]
        }).compileComponents();

        component = TestBed.get(ProfileComponent);
        userService = TestBed.get(UserService);
    });

    it("should create", async () => {
        expect(component).toBeTruthy();
        expect(component.isLoad).toBeFalsy();
    });

    it("should run #ngOnInit()", async () => {
        spyOn(userService, "getUserState").and.returnValue(of(new User({
            _id: "",
            email: "test.test@epitech.eu",
            password: "",
            avatar: "",
            pseudo: "Test",
            admin: false,
            cryptocurrencies: [
                new Cryptocurrencies(null)
            ],
            defaultCurrency: Currency.EUR,
            filters: [],
            timestamp: "",
            token: ""
        })));
        component.ngOnInit();
        expect(component.isLoad).toBeTruthy();
    });

    it("should run #doUpdate() already on updating", async () => {
        const event = new MouseEvent("submit");
        component.onUpdate = true;
        component.doUpdate(event);
        expect(component.error === "").toBeTruthy();
    });

    it("should run #doUpdate() no modify", async () => {
        const event = new MouseEvent("submit");
        component.user = new User();
        component.userSaved = new User();
        component.user.pseudo = "Test";
        component.userSaved.pseudo = "Test";
        component.user.filters = [];
        component.userSaved.filters = [];
        component.doUpdate(event);
        expect(component.error === "No data changed.").toBeTruthy();
    });

    it("should run #doUpdate() empty pseudo", async () => {
        const event = new MouseEvent("submit");
        component.user = new User();
        component.userSaved = new User();
        component.user.pseudo = "";
        component.userSaved.pseudo = "Test";
        component.user.filters = [];
        component.doUpdate(event);
        expect(component.error === "Please provide a value for all fields.").toBeTruthy();
    });

    it("should run #doUpdate() empty email", async () => {
        const event = new MouseEvent("submit");
        component.user = new User();
        component.userSaved = new User();
        component.user.pseudo = "Test";
        component.userSaved.pseudo = "Test";
        component.user.email = "";
        component.userSaved.email = "test.test@epitech.eu";
        component.user.filters = [];
        component.doUpdate(event);
        expect(component.error === "Please provide a value for all fields.").toBeTruthy();
    });

    it("should run #doUpdate() update fail", async () => {
        const event = new MouseEvent("submit");
        component.user = new User();
        component.userSaved = new User();
        component.user.pseudo = "TestChanged";
        component.userSaved.pseudo = "Test";
        component.user.email = "testChanged.test@epitech.eu";
        component.userSaved.email = "test.test@epitech.eu";
        component.user.filters = ["TestChanged"];
        component.userSaved.filters = ["Test"];
        spyOn(userService, "profileUser").and.returnValue(throwError({ status: 401 }));
        component.doUpdate(event);
        expect(component.error === "Update profile: An error occure during profile.").toBeTruthy();
    });

    it("should run #doUpdate() update done", async () => {
        const event = new MouseEvent("submit");
        component.user = new User();
        component.userSaved = new User();
        component.user.pseudo = "TestChanged";
        component.userSaved.pseudo = "Test";
        component.user.email = "testChanged.test@epitech.eu";
        component.userSaved.email = "test.test@epitech.eu";
        component.user.filters = ["TestChanged", ""];
        component.userSaved.filters = ["Test"];
        spyOn(userService, "profileUser").and.returnValue(of(""));
        component.doUpdate(event);
        expect(component.onUpdate === false).toBeTruthy();
    });

    it("should run #getCurrenciesKeys()", async () => {
        expect(isEqual(component.getCurrenciesKeys(), [{ key: 1, value: "EUR" }, { key: 2, value: "USD" }])).toBeTruthy();
    });

    it("should run #addFilter()", async () => {
        const event = new MouseEvent("submit");
        component.user = new User();
        component.user.filters = [];
        component.addFilter(event);
        expect(isEqual(component.user.filters, [""])).toBeTruthy();
    });

    it("should run #deleteFilter()", async () => {
        const event = new MouseEvent("submit");
        component.user = new User();
        component.user.filters = [""];
        component.deleteFilter(event, 0);
        expect(isEqual(component.user.filters, [])).toBeTruthy();
    });

    it("should run #trackByIndex()", async () => {
        expect(component.trackByIndex(0, null) === 0).toBeTruthy();
    });

    it("should run #changeAvatar() bad type", async () => {
        component.changeAvatar({
            target: {
                files: [
                    new File([""], "TestFile.png", { type: "application/zip" })
                ]
            }
        });
        expect(component.error === "Upload avatar: Only png file type supported.").toBeTruthy();
    });

    it("should run #changeAvatar() good type", async () => {
        component.user = new User();
        component.user.avatar = "";
        component.changeAvatar({
            target: {
                files: [
                    new File(["data:image/png;base64,"], "TestFile.png", { type: "image/png" })
                ]
            }
        });
        expect(component.error === "").toBeTruthy();
    });

});
