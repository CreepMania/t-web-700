import { TestBed } from "@angular/core/testing";
import { GuidedTourService } from "ngx-guided-tour";
import { of, throwError } from "rxjs";
import { Articles } from "src/app/common/interfaces/iarticles";
import { Cryptocurrencies } from "src/app/common/interfaces/icryptocurrencies";
import { Currency, User } from "src/app/common/interfaces/iusers";
import { ArticlesService } from "src/app/common/services/articles.service";
import { UserService } from "src/app/common/services/users.service";
import { HomeComponent } from "src/app/components/home/home.component";
import { instance, mock } from "ts-mockito";

let articlesServiceMock: ArticlesService;
let userServiceMock: UserService;

describe("HomeComponent", () => {
    let component: HomeComponent;
    let articlesService: ArticlesService;
    let userService: UserService;

    beforeEach(() => {
        articlesServiceMock = mock(ArticlesService);
        userServiceMock = mock(UserService);

        TestBed.configureTestingModule({
            providers: [
                HomeComponent,
                { provide: ArticlesService, useValue: instance(articlesServiceMock) },
                { provide: GuidedTourService },
                { provide: UserService, useValue: instance(userServiceMock)}
            ]
        }).compileComponents();

        component = TestBed.get(HomeComponent);
        articlesService = TestBed.get(ArticlesService);
        userService = TestBed.get(UserService);
    });

    it("should create", () => {
        expect(component).toBeTruthy();
    });

    it("should run #ngOnInit() with server error", async () => {
        spyOn(articlesService, "getAllArticles").and.returnValue(throwError({ status: 404 }));
        component.ngOnInit();
        expect(component.isLoaded).toBeFalsy();
    });

    it("should run #ngOnInit() without server error", async () => {
        spyOn(articlesService, "getAllArticles").and.returnValue(of([new Articles({
            _id: "",
            guid: "",
            published_on: 0,
            imageurl: "",
            title: "",
            url: "",
            source: "",
            body: "",
            tags: "",
            categories: "",
            upvotes: "",
            downvotes: "",
            lang: "",
            source_info: []
        })]));
        component.ngOnInit();
        expect(component.isLoaded).toBeTruthy();
    });

    it("should run #onClickFilter() without filters and with server error", async () => {
        spyOn(articlesService, "getAllArticles").and.returnValue(throwError({ status: 404 }));
        const user = new User({
            _id: "",
            email: "test.test@epitech.eu",
            password: "",
            avatar: "",
            pseudo: "Test",
            admin: false,
            cryptocurrencies: [
                new Cryptocurrencies(null)
            ],
            defaultCurrency: Currency.EUR,
            filters: [],
            timestamp: "",
            token: ""
        });
        userService.loggedUser = user;
        component.isFilterActivated = false;
        component.onClickFilter();
        expect(component.isFilterActivated).toBeTruthy();
        expect(component.error === "An error occure during the call of articles API. Please retry later.").toBeTruthy();
        expect(component.isLoaded).toBeFalsy();
    });

    it("should run #onClickFilter() with filters and without server error", async () => {
        spyOn(articlesService, "getAllArticles").and.returnValue(of([new Articles({
            _id: "",
            guid: "",
            published_on: 0,
            imageurl: "",
            title: "",
            url: "",
            source: "",
            body: "",
            tags: "",
            categories: "",
            upvotes: "",
            downvotes: "",
            lang: "",
            source_info: []
        })]));
        const user = new User({
            _id: "",
            email: "test.test@epitech.eu",
            password: "",
            avatar: "",
            pseudo: "Test",
            admin: false,
            cryptocurrencies: [
                new Cryptocurrencies(null)
            ],
            defaultCurrency: Currency.EUR,
            filters: [],
            timestamp: "",
            token: ""
        });
        userService.loggedUser = user;
        component.isFilterActivated = true;
        component.onClickFilter();
        expect(component.isFilterActivated).toBeFalsy();
        expect(component.isLoaded).toBeTruthy();
    });
});
