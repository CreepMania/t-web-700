import { Component, OnInit } from "@angular/core";
import { GuidedTourService } from "ngx-guided-tour";
import { HelperComponent } from "src/app/common/helper/helper.component";
import { Articles } from "src/app/common/interfaces/iarticles";
import { ArticlesService } from "src/app/common/services/articles.service";
import { UserService } from "src/app/common/services/users.service";

/**
 * Home component of application (articles management).
 */
@Component({
    selector: "app-home",
    templateUrl: "./home.component.html",
    styleUrls: ["./home.component.scss"]
})

export class HomeComponent extends HelperComponent implements OnInit {

    /**
     * Boolean who say if the articles are loaded, it's used to display the loader element.
     */
    public isLoaded = false;

    /**
     * Boolean who say if the article filter is activated.
     */
    public isFilterActivated = false;

    /**
     * Contain the list of articles.
     */
    public articlesList: Articles[] = [];

    /**
     * Contains form error.
     */
    public error = "";

    /**
     * Constructor of component.
     */
    constructor(private articlesService: ArticlesService, private userService: UserService, guidedTourService: GuidedTourService) {
        super(guidedTourService);
    }

    /**
     * Method called when component is on initialisation.
     */
    public ngOnInit() {
        this.articlesService.getAllArticles().subscribe((s) => {
            this.articlesList = s;
            this.isLoaded = true;
            this.articlesList.forEach((obj: Articles) => {
                obj.published_on_date = new Date(obj.published_on * 1000);
                if (obj.published_on_date.getDate() !== new Date(Date.now()).getDate()) {
                    obj.published_today = 0;
                } else {
                    obj.published_today = 1;
                }
            });
        }, (err) => {
            this.error = "An error occure during the call of articles API. Please retry later.";
        });
    }

    /**
     * Method called when user click on filter.
     */
    public onClickFilter() {
        this.isLoaded = false;
        if (!this.isFilterActivated) {
            this.isFilterActivated = true;
        } else {
            this.isFilterActivated = false;
        }
        this.articlesService.getAllArticles(this.isFilterActivated ? this.userService.loggedUser.filters : []).subscribe((s) => {
            this.articlesList = s;
            this.isLoaded = true;
            this.articlesList.forEach((obj: Articles) => {
                obj.published_on_date = new Date(obj.published_on * 1000);
                if (obj.published_on_date.getDate() !== new Date(Date.now()).getDate()) {
                    obj.published_today = 0;
                } else {
                    obj.published_today = 1;
                }
            });
        }, (err) => {
            this.error = "An error occure during the call of articles API. Please retry later.";
        });
    }

}
