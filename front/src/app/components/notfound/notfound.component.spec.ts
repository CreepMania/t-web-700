import { TestBed } from "@angular/core/testing";
import { NotfoundComponent } from "src/app/components/notfound/notfound.component";

describe("NotfoundComponent", () => {
    let component: NotfoundComponent;

    beforeEach(() => {

        TestBed.configureTestingModule({
            providers: [
                NotfoundComponent
            ]
        }).compileComponents();

        component = TestBed.get(NotfoundComponent);
    });

    it("should create", async () => {
        expect(component).toBeTruthy();
    });

});
