import { Component } from "@angular/core";

/**
 * Component to redirect if no page found.
 */
@Component({
    selector: "app-notfound",
    templateUrl: "./notfound.component.html",
    styleUrls: ["./notfound.component.scss"]
})

export class NotfoundComponent {

    /**
     * Constructor of NotfoundComponent.
     */
    constructor() { }

}
