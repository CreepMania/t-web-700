import { DatePipe } from "@angular/common";
import { TestBed } from "@angular/core/testing";
import { FormsModule } from "@angular/forms";
import { isEqual } from "lodash";
import { ChartsModule } from "ng2-charts";
import { GuidedTourService } from "ngx-guided-tour";
import { of, throwError } from "rxjs";
import { Cryptocurrencies, Cryptohisto, Data, DataEntity } from "src/app/common/interfaces/icryptocurrencies";
import { Period } from "src/app/common/interfaces/iperiod";
import { Currency, User } from "src/app/common/interfaces/iusers";
import { CryptosService } from "src/app/common/services/cryptos.service";
import { UserService } from "src/app/common/services/users.service";
import { CryptosComponent } from "src/app/components/cyptos/cryptos.component";
import { instance, mock } from "ts-mockito";

let cryptosServiceMock: CryptosService;
let userServiceMock: UserService;

describe("CryptosComponent", () => {
    let cryptosService: CryptosService;
    let userService: UserService;
    let component: CryptosComponent;

    beforeEach(() => {
        cryptosServiceMock = mock(CryptosService);
        userServiceMock = mock(UserService);

        TestBed.configureTestingModule({
            providers: [
                CryptosComponent,
                DatePipe,
                { provide: CryptosService, useValue: instance(cryptosServiceMock) },
                { provide: UserService, useValue: instance(userServiceMock) },
                { provide: GuidedTourService }
            ],
            imports: [
                FormsModule,
                ChartsModule
            ]
        }).compileComponents();

        component = TestBed.get(CryptosComponent);
        cryptosService = TestBed.get(CryptosService);
        userService = TestBed.get(UserService);
    });

    it("should create", async () => {
        expect(component).toBeTruthy();
    });

    it("should run #ngOnInit() with error on getCryptos method", async () => {
        spyOn(userService, "getUserState").and.returnValue(of(new User({
            _id: "",
            email: "test.test@epitech.eu",
            password: "",
            avatar: "",
            pseudo: "Test",
            admin: false,
            cryptocurrencies: [
                new Cryptocurrencies(null)
            ],
            defaultCurrency: Currency.EUR,
            filters: [],
            timestamp: "",
            token: ""
        })));
        spyOn(cryptosService, "getCryptos").and.returnValue(throwError({ status: 404 }));
        component.ngOnInit();
        expect(component.error === "An error occure during cryptos get. Please retry later.").toBeTruthy();
    });

    it("should run #ngOnInit() with anonymous user", async () => {
        spyOn(userService, "getUserState").and.returnValue(of(null));
        spyOn(cryptosService, "getCryptos").and.returnValue(of([
            new Cryptocurrencies({
                _id: "",
                name: "Test",
                logo: "Test",
                enable: false,
                shortName: "Test",
                eur: 0,
                usd: 0
            })
        ]));
        spyOn(cryptosService, "getHisto").and.returnValue(of(new Cryptohisto({
            _id: "",
            enable: true,
            logo: "",
            name: "",
            shortName: "",
            data: new Data({
                Aggregated: true,
                TimeFrom: 0,
                TimeTo: 0,
                Data: [new DataEntity({
                    time: 0,
                    high: 10,
                    low: 0,
                    open: 0,
                    close: 0,
                    conversionSymbol: "",
                    conversionType: "",
                    volumefrom: 0,
                    volumeto: 0
                })]
            })
        })));
        component.ngOnInit();
        expect(component.isLoad).toBeTruthy();
    });

    it("should run #ngOnInit() without errors empty cryptos", async () => {
        spyOn(userService, "getUserState").and.returnValue(of(new User({
            _id: "",
            email: "test.test@epitech.eu",
            password: "",
            avatar: "",
            pseudo: "Test",
            admin: false,
            cryptocurrencies: [
                new Cryptocurrencies(null)
            ],
            defaultCurrency: Currency.EUR,
            filters: [],
            timestamp: "",
            token: ""
        })));
        spyOn(cryptosService, "getCryptos").and.returnValue(of([]));
        component.ngOnInit();
        expect(component.isLoad).toBeTruthy();
    });

    it("should run #ngOnInit() without errors and with cryptos", async () => {
        spyOn(userService, "getUserState").and.returnValue(of(new User({
            _id: "",
            email: "test.test@epitech.eu",
            password: "",
            avatar: "",
            pseudo: "Test",
            admin: false,
            cryptocurrencies: [
                new Cryptocurrencies(null)
            ],
            defaultCurrency: Currency.EUR,
            filters: [],
            timestamp: "",
            token: ""
        })));
        spyOn(cryptosService, "getCryptos").and.returnValue(of([
            new Cryptocurrencies({
                _id: "",
                name: "Test",
                logo: "Test",
                enable: false,
                shortName: "Test",
                eur: 0,
                usd: 0
            })
        ]));
        spyOn(cryptosService, "getHisto").and.returnValue(of(new Cryptohisto({
            _id: "",
            enable: true,
            logo: "",
            name: "",
            shortName: "",
            data: new Data({
                Aggregated: true,
                TimeFrom: 0,
                TimeTo: 0,
                Data: [new DataEntity({
                    time: 0,
                    high: 10,
                    low: 0,
                    open: 0,
                    close: 0,
                    conversionSymbol: "",
                    conversionType: "",
                    volumefrom: 0,
                    volumeto: 0
                })]
            })
        })));
        component.ngOnInit();
        expect(component.isLoad).toBeTruthy();
    });

    it("should run #ngOnInit() without errors and with cryptos", async () => {
        spyOn(userService, "getUserState").and.returnValue(of(new User({
            _id: "",
            email: "test.test@epitech.eu",
            password: "",
            avatar: "",
            pseudo: "Test",
            admin: false,
            cryptocurrencies: [
                new Cryptocurrencies(null)
            ],
            defaultCurrency: Currency.EUR,
            filters: [],
            timestamp: "",
            token: ""
        })));
        spyOn(cryptosService, "getCryptos").and.returnValue(of([
            new Cryptocurrencies({
                _id: "",
                name: "Test",
                logo: "Test",
                enable: false,
                shortName: "Test",
                eur: 0,
                usd: 0
            })
        ]));
        spyOn(cryptosService, "getHisto").and.returnValue(throwError({ status: 403 }));
        component.ngOnInit();
        expect(isEqual(component.error, "An error occure during history get. Please retry later.")).toBeTruthy();
    });

    it("should run #updateChart() with Day Period", async () => {
        const entity = new DataEntity(null);
        const cryptoHisto = new Cryptohisto();
        cryptoHisto.data = new Data({
            Aggregated: true,
            TimeFrom: 0,
            TimeTo: 0,
            Data: [new DataEntity({
                time: 0,
                high: 10,
                low: 0,
                open: 0,
                close: 0,
                conversionSymbol: "",
                conversionType: "",
                volumefrom: 0,
                volumeto: 0
            })]
        });
        component.cryptosHistory = [cryptoHisto];
        component.chartsData.push([]);
        component.chartsLabels.push([]);
        component.updateChart(0);
        expect(isEqual(component.chartsData[0], [{
            data: [10], label: "high", scaleFontColor: "#FFFFFF",
        }, {
            data: [0], label: "low", scaleFontColor: "#FFFFFF",
        }])).toBeTruthy();
        expect(isEqual(component.chartsLabels[0][0].length, 10)).toBeTruthy();
    });

    it("should run #updateChart() with Hour Period", async () => {
        const entity = new DataEntity(null);
        const cryptoHisto = new Cryptohisto();
        cryptoHisto.data = new Data({
            Aggregated: true,
            TimeFrom: 0,
            TimeTo: 0,
            Data: [new DataEntity({
                time: 0,
                high: 10,
                low: 0,
                open: 0,
                close: 0,
                conversionSymbol: "",
                conversionType: "",
                volumefrom: 0,
                volumeto: 0
            })]
        });
        component.cryptosHistory = [cryptoHisto];
        component.chartsData.push([]);
        component.chartsLabels.push([]);
        component.period = Period.Hour;
        component.updateChart(0);
        expect(isEqual(component.chartsData[0], [{
            data: [10], label: "high", scaleFontColor: "#FFFFFF",
        }, {
            data: [0], label: "low", scaleFontColor: "#FFFFFF",
        }])).toBeTruthy();
        expect(isEqual(component.chartsLabels[0][0].length, 5)).toBeTruthy();
    });

    it("should run #updateChart() with Minute Period", async () => {
        const entity = new DataEntity(null);
        const cryptoHisto = new Cryptohisto();
        cryptoHisto.data = new Data({
            Aggregated: true,
            TimeFrom: 0,
            TimeTo: 0,
            Data: [new DataEntity({
                time: 0,
                high: 10,
                low: 0,
                open: 0,
                close: 0,
                conversionSymbol: "",
                conversionType: "",
                volumefrom: 0,
                volumeto: 0
            })]
        });
        component.cryptosHistory = [cryptoHisto];
        component.chartsData.push([]);
        component.chartsLabels.push([]);
        component.period = Period.Minute;
        component.updateChart(0);
        expect(isEqual(component.chartsData[0], [{
            data: [10], label: "high", scaleFontColor: "#FFFFFF",
        }, {
            data: [0], label: "low", scaleFontColor: "#FFFFFF",
        }])).toBeTruthy();
        expect(isEqual(component.chartsLabels[0][0].length, 5)).toBeTruthy();
    });

    it("should run #applyCriteria()", async () => {
        component.loggedUser = new User({
            _id: "",
            email: "test.test@epitech.eu",
            password: "",
            avatar: "",
            pseudo: "Test",
            admin: false,
            cryptocurrencies: [
                new Cryptocurrencies(null)
            ],
            defaultCurrency: Currency.EUR,
            filters: [],
            timestamp: "",
            token: ""
        });
        const cryptoHisto = new Cryptohisto({
            _id: "",
            name: "",
            shortName: "",
            logo: "",
            enable: true,
            data: new Data({
                Aggregated: true,
                TimeFrom: 0,
                TimeTo: 0,
                Data: [new DataEntity({
                    time: 0,
                    high: 10,
                    low: 0,
                    open: 0,
                    close: 0,
                    conversionSymbol: "",
                    conversionType: "",
                    volumefrom: 0,
                    volumeto: 0
                })]
            })
        });
        cryptoHisto.data = new Data();
        cryptoHisto.data.Data = [];
        spyOn(cryptosService, "getHisto").and.returnValue(of(new Cryptohisto({
            _id: "",
            enable: true,
            logo: "",
            name: "",
            shortName: "",
            data: new Data({
                Aggregated: true,
                TimeFrom: 0,
                TimeTo: 0,
                Data: [new DataEntity({
                    time: 0,
                    high: 10,
                    low: 0,
                    open: 0,
                    close: 0,
                    conversionSymbol: "",
                    conversionType: "",
                    volumefrom: 0,
                    volumeto: 0
                })]
            })
        })));
        component.cryptosHistory = [cryptoHisto, null];
        component.chartsData.push([]);
        component.chartsLabels.push([]);
        component.applyCriteria();
        expect(component.isLoad).toBeTruthy();
    });

    it("should run #getPeriodKeys()", async () => {
        expect(isEqual(component.getPeriodKeys(), [{ key: "m", value: "Minute" }, { key: "h", value: "Hour" }, { key: "d", value: "Day" }])).toBeTruthy();
    });

});
