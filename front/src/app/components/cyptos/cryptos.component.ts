import { DatePipe } from "@angular/common";
import { Component, OnInit } from "@angular/core";
import { GuidedTourService } from "ngx-guided-tour";
import { HelperComponent } from "src/app/common/helper/helper.component";
import { Cryptocurrencies, Cryptohisto, DataEntity } from "src/app/common/interfaces/icryptocurrencies";
import { Period } from "src/app/common/interfaces/iperiod";
import { Currency, User } from "src/app/common/interfaces/iusers";
import { CryptosService } from "src/app/common/services/cryptos.service";
import { UserService } from "src/app/common/services/users.service";

/**
 * Cryptos component of application.
 */
@Component({
    selector: "app-cryptos",
    templateUrl: "./cryptos.component.html",
    styleUrls: ["./cryptos.component.scss"]
})

export class CryptosComponent extends HelperComponent implements OnInit {

    /**
     * Property to store logged user.
     */
    public loggedUser: User = null;

    /**
     * Property to store if component is load or not.
     */
    public isLoad = false;

    /**
     * Contains errors.
     */
    public error = "";

    /**
     * Contains all cryptos enabled.
     */
    public cryptos: Cryptocurrencies[] = [];

    /**
     * Contains all history of crypto.
     */
    public cryptosHistory: Cryptohisto[] = [];

    /**
     * Contains value of pariod.
     */
    public periodValue = 30;

    /**
     * Contains type of period (Minutes / Hours / Days)
     */
    public period: Period = Period.Day;

    /**
     * Contains default charts options (white font).
     */
    public chartOptions = {
        responsive: true,
        scales: {
            xAxes: [{
                ticks: {
                    fontColor: "#ffffff"
                },
                gridLines: {
                    color: "#b5b5b5"
                }
            }],
            yAxes: [{
                ticks: {
                    fontColor: "#ffffff"
                },
                gridLines: {
                    color: "#b5b5b5"
                }
            }]
        },
        legend: {
            display: true,
            labels: {
                fontColor: "#ffffff"
            },
        }
    };

    /**
     * Contains datas to show on charts.
     * chartsData[index] contains an array of number values. (Prices)
     */
    public chartsData = [];

    /**
     * Contains labels to show on charts.
     * chartLabels[index] contains an array of string values. (Dates)
     */
    public chartsLabels = [];

    /**
     * Constructor of component.
     */
    constructor(private cryptosService: CryptosService, private userService: UserService, private datepipe: DatePipe, guidedTourService: GuidedTourService) {
        super(guidedTourService);
    }

    /**
     * Method called on init of component.
     */
    public ngOnInit() {
        this.userService.getUserState().subscribe((u: User) => {
            const cryptos = [];
            if (u != null) {
                this.loggedUser = u;
                this.loggedUser.cryptocurrencies.forEach((crypto: Cryptocurrencies) => {
                    cryptos.push(crypto);
                });
            }
            this.cryptosService.getCryptos(cryptos).subscribe((s: Cryptocurrencies[]) => {
                this.cryptos = s;
                this.cryptos.forEach((element) => {
                    this.cryptosHistory.push(null);
                    this.chartsData.push([]);
                    this.chartsLabels.push([]);
                });
                if (this.cryptos.length > 0 && u != null) {
                    this.getHisto(this.cryptos[0].shortName, 0);
                }
                this.isLoad = true;
            }, (err) => {
                this.error = "An error occure during cryptos get. Please retry later.";
            });
        });
    }

    /**
     * Method to get histo of a currency.
     * @param cmid Id of crypto currency.
     * @param index Index of crypto currency.
     */
    public getHisto(cmid: string, index: number) {
        this.chartsData[index] = [];
        this.chartsLabels[index] = [];
        let defaultCurrency = "EUR";
        if (this.loggedUser) {
            defaultCurrency = Object.keys(Currency).filter((x) => this.loggedUser.defaultCurrency === Currency[x])[0];
            this.cryptosService.getHisto(cmid, this.periodValue.toString() + this.period, defaultCurrency).subscribe((s: Cryptohisto) => {
                this.cryptosHistory[index] = s;
                this.updateChart(index);
            }, (err) => {
                this.error = "An error occure during history get. Please retry later.";
            });
        }
    }

    /**
     * Method to update chart datas.
     * @param index Index of chart to update.
     */
    public updateChart(index: number) {
        const dataHigh = [];
        const dataLow = [];
        const label = [];
        this.cryptosHistory[index].data.Data.forEach((element: DataEntity) => {
            dataHigh.push(element.high);
            dataLow.push(element.low);
            const date = new Date(element.time * 1000);
            switch (this.period) {
                case Period.Day:
                    label.push(this.datepipe.transform(date, "yyyy/MM/dd"));
                    break;
                case Period.Hour:
                case Period.Minute:
                    label.push(this.datepipe.transform(date, "hh:mm"));
                    break;
            }
        });
        this.chartsData[index] = [{
            data: dataHigh, label: "high", scaleFontColor: "#FFFFFF",
        }, {
            data: dataLow, label: "low", scaleFontColor: "#FFFFFF",
        }];
        this.chartsLabels[index] = label;
    }

    /**
     * Method to apply criteria.
     */
    public applyCriteria() {
        this.isLoad = false;
        for (let i = 0; i < this.cryptosHistory.length; i++) {
            if (this.cryptosHistory[i] != null) {
                this.getHisto(this.cryptosHistory[i].shortName, i);
            }
        }
        this.isLoad = true;
    }

    /**
     * Get all keys of Period enum for combobox.
     */
    public getPeriodKeys(): any[] {
        const result = [];
        Object.keys(Period).filter((key) => typeof Period[key as any] === "string").forEach((key) => {
            result.push({
                key: Period[key],
                value: key
            });
        });
        return result;
    }

}
