import { Component, OnInit } from "@angular/core";
import { GuidedTourService } from "ngx-guided-tour";
import { ToastrService } from "ngx-toastr";
import { HelperComponent } from "src/app/common/helper/helper.component";
import { Cryptocurrencies } from "src/app/common/interfaces/icryptocurrencies";
import { CryptosService } from "src/app/common/services/cryptos.service";
import { UserService } from "src/app/common/services/users.service";

/**
 * Component to manage web site.
 */
@Component({
    selector: "app-admin",
    templateUrl: "./admin.component.html",
    styleUrls: ["./admin.component.scss"]
})

export class AdminComponent extends HelperComponent implements OnInit {

    /**
     * Store list of cryptos.
     */
    public cryptos: Cryptocurrencies[] = [];

    /**
     * Define if an action is pending (call to back end).
     */
    public onAction = true;

    /**
     * Constructor of AdminComponent.
     * @param userService Service of users.
     * @param toastr Toasts service to show toasts.
     */
    constructor(
        private userService: UserService,
        private cryptosService: CryptosService,
        private toastr: ToastrService,
        guidedTourService: GuidedTourService) {
        super(guidedTourService);
    }

    /**
     * Method called on initialization on component.
     */
    public ngOnInit() {
        this.cryptosService.getCryptos([]).subscribe((s: Cryptocurrencies[]) => {
            this.cryptos = s;
            this.onAction = false;
        });
    }

    /**
     * Trackby method to prevent DOM redraw.
     * @param index index of filter.
     * @param obj obj of filter.
     */
    public trackByIndex(index: number, obj: any): any {
        return index;
    }

    /**
     * Method handled on button "Add crypto" click.
     * @param event mouse event to prevent default form post.
     */
    public addCrypto(event: MouseEvent): void {
        event.preventDefault();
        if (this.cryptos.length > 0 && !this.cryptos[this.cryptos.length - 1]._id) {
            this.toastr.warning("You must complete the last crypto add action before and save her.");
            return;
        }
        this.toastr.info("You added a new crypto, don't forget to save.", "Add crypto");
        this.cryptos.push(new Cryptocurrencies({
            _id: null,
            name: null,
            logo: null,
            shortName: null,
            enable: true
        }));
    }

    /**
     * Save a crypto (add her if no _id or modify her).
     * @param event mouse event to prevent default action (redirect).
     * @param index index of crypto to save.
     */
    public saveCrypto(event: MouseEvent, index: number): void {
        event.preventDefault();
        this.onAction = true;
        if (!this.cryptos[index].name || !this.cryptos[index].shortName || !this.cryptos[index].logo) {
            this.toastr.warning("Please provide a value for all fields");
            this.onAction = false;
            return;
        }
        if (this.cryptos[index]._id) {
            this.cryptosService.updateCryptos(this.cryptos[index]).subscribe((s) => {
                this.toastr.success("You correctly updated your crypto.", "Update crypto");
                this.onAction = false;
            }, (err) => {
                this.toastr.error("An error occured during crypto update.", "Update crypto");
                this.onAction = false;
            });
        } else {
            this.cryptosService.createCryptos(this.cryptos[index]).subscribe((s: Cryptocurrencies) => {
                this.cryptos[index] = s;
                this.toastr.success("You correctly added this crypto.", "Create crypto");
                this.onAction = false;
            }, (err) => {
                this.toastr.error("An error occured during crypto creation.", "Create crypto");
                this.onAction = false;
            });
        }
    }

    /**
     * Method to delete a crypto.
     * @param event mouse event to prevent default action.
     * @param index index of crypto to delete.
     */
    public deleteCrypto(event: MouseEvent, index: number) {
        event.preventDefault();
        this.onAction = true;
        this.cryptosService.deleteCryptos(this.cryptos[index]).subscribe((s: Boolean) => {
            if (s.valueOf()) {
                this.toastr.success("You correctly deleted this crypto.", "Delete crypto");
                const removeElementIndex = this.cryptos.indexOf(this.cryptos[index]);
                this.cryptos.splice(removeElementIndex, 1);
            } else {
                this.toastr.error("An error occured during crypto deletion.", "Delete crypto");
            }
            this.onAction = false;
        }, (err) => {
            this.toastr.error("An error occured during crypto deletion.", "Delete crypto");
            this.onAction = false;
        });
    }

    /**
     * Method to cancel crypto creation.
     * @param event mouse event to prevent default action.
     * @param index index of crypto to cancel creation.
     */
    public cancelCrypto(event: MouseEvent, index: number) {
        event.preventDefault();
        const removeElementIndex = this.cryptos.indexOf(this.cryptos[index]);
        this.cryptos.splice(removeElementIndex, 1);
    }

}
