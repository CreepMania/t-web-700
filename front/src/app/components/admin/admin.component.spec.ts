import { TestBed } from "@angular/core/testing";
import { isEqual } from "lodash";
import { GuidedTourService } from "ngx-guided-tour";
import { ToastrModule, ToastrService } from "ngx-toastr";
import { of, throwError } from "rxjs";
import { Cryptocurrencies } from "src/app/common/interfaces/icryptocurrencies";
import { CryptosService } from "src/app/common/services/cryptos.service";
import { UserService } from "src/app/common/services/users.service";
import { AdminComponent } from "src/app/components/admin/admin.component";
import { instance, mock } from "ts-mockito";

let userServiceMock: UserService;
let toastrServiceMock: ToastrService;
let guidedTourServiceMock: GuidedTourService;
let cryptosServiceMock: CryptosService;

describe("AdminComponent", () => {
    let userService: UserService;
    let component: AdminComponent;
    let cryptosService: CryptosService;

    beforeEach(() => {
        userServiceMock = mock(UserService);
        toastrServiceMock = mock(ToastrService);
        guidedTourServiceMock = mock(GuidedTourService);
        cryptosServiceMock = mock(CryptosService);

        TestBed.configureTestingModule({
            providers: [
                AdminComponent,
                { provide: UserService, useValue: instance(userServiceMock) },
                { provide: ToastrService, useValue: instance(toastrServiceMock) },
                { provide: GuidedTourService, useValue: instance(guidedTourServiceMock) },
                { provide: CryptosService, useValue: instance(cryptosServiceMock) }
            ],
            imports: [
                ToastrModule
            ]
        }).compileComponents();

        component = TestBed.get(AdminComponent);
        userService = TestBed.get(UserService);
        cryptosService = TestBed.get(CryptosService);
    });

    it("should create", async () => {
        expect(component).toBeTruthy();
    });

    it("should run #ngOnInit()", async () => {
        spyOn(cryptosService, "getCryptos").and.returnValue(of([
            new Cryptocurrencies({
                _id: "",
                name: "Test",
                logo: "Test",
                enable: false,
                shortName: "Test",
                eur: 0,
                usd: 0
            })
        ]));
        component.ngOnInit();
        expect(component.onAction).toBeFalsy();
    });

    it("should run #trackByIndex()", async () => {
        expect(component.trackByIndex(0, null) === 0).toBeTruthy();
    });

    it("should run #addCrypto()", async () => {
        const event = new MouseEvent("submit");
        component.cryptos = [];
        component.addCrypto(event);
        expect(isEqual(component.cryptos, [new Cryptocurrencies({
            _id: null,
            name: null,
            logo: null,
            shortName: null,
            enable: true
        })])).toBeTruthy();
        component.addCrypto(event);
        expect(isEqual(component.cryptos, [new Cryptocurrencies({
            _id: null,
            name: null,
            logo: null,
            shortName: null,
            enable: true
        })])).toBeTruthy();
    });

    it("should run #saveCrypto() check name null", async () => {
        const event = new MouseEvent("submit");
        component.cryptos = [new Cryptocurrencies({
            _id: null,
            name: null,
            logo: null,
            shortName: null,
            enable: true
        })];
        component.saveCrypto(event, 0);
        expect().nothing();
    });

    it("should run #saveCrypto() check short name null", async () => {
        const event = new MouseEvent("submit");
        component.cryptos = [new Cryptocurrencies({
            _id: null,
            name: "Test",
            logo: null,
            shortName: null,
            enable: true
        })];
        component.saveCrypto(event, 0);
        expect().nothing();
    });

    it("should run #saveCrypto() check logo null", async () => {
        const event = new MouseEvent("submit");
        component.cryptos = [new Cryptocurrencies({
            _id: null,
            name: "Test",
            logo: null,
            shortName: "Test",
            enable: true
        })];
        component.saveCrypto(event, 0);
        expect().nothing();
    });

    it("should run #saveCrypto() update without error", async () => {
        const event = new MouseEvent("submit");
        component.cryptos = [new Cryptocurrencies({
            _id: "Test",
            name: "Test",
            logo: "Test",
            shortName: "Test",
            enable: true
        })];
        spyOn(cryptosService, "updateCryptos").and.returnValue(of({}));
        component.saveCrypto(event, 0);
        expect().nothing();
    });

    it("should run #saveCrypto() update with error", async () => {
        const event = new MouseEvent("submit");
        component.cryptos = [new Cryptocurrencies({
            _id: "Test",
            name: "Test",
            logo: "Test",
            shortName: "Test",
            enable: true
        })];
        spyOn(cryptosService, "updateCryptos").and.returnValue(throwError({ status: 404 }));
        component.saveCrypto(event, 0);
        expect().nothing();
    });

    it("should run #saveCrypto() create without error", async () => {
        const event = new MouseEvent("submit");
        component.cryptos = [new Cryptocurrencies({
            _id: null,
            name: "Test",
            logo: "Test",
            shortName: "Test",
            enable: true
        })];
        spyOn(cryptosService, "createCryptos").and.returnValue(of(component.cryptos[0]));
        component.saveCrypto(event, 0);
        expect().nothing();
    });

    it("should run #saveCrypto() create with error", async () => {
        const event = new MouseEvent("submit");
        component.cryptos = [new Cryptocurrencies({
            _id: null,
            name: "Test",
            logo: "Test",
            shortName: "Test",
            enable: true
        })];
        spyOn(cryptosService, "createCryptos").and.returnValue(throwError({ status: 404 }));
        component.saveCrypto(event, 0);
        expect().nothing();
    });

    it("should run #deleteCrypto() without error true", async () => {
        const event = new MouseEvent("submit");
        component.cryptos = [new Cryptocurrencies({
            _id: "Test",
            name: "Test",
            logo: "Test",
            shortName: "Test",
            enable: true
        })];
        spyOn(cryptosService, "deleteCryptos").and.returnValue(of(true));
        component.deleteCrypto(event, 0);
        expect(isEqual(component.cryptos, [])).toBeTruthy();
    });

    it("should run #deleteCrypto() without error false", async () => {
        const event = new MouseEvent("submit");
        component.cryptos = [new Cryptocurrencies({
            _id: "Test",
            name: "Test",
            logo: "Test",
            shortName: "Test",
            enable: true
        })];
        spyOn(cryptosService, "deleteCryptos").and.returnValue(of(false));
        component.deleteCrypto(event, 0);
        expect().nothing();
    });

    it("should run #deleteCrypto() with error", async () => {
        const event = new MouseEvent("submit");
        component.cryptos = [new Cryptocurrencies({
            _id: "Test",
            name: "Test",
            logo: "Test",
            shortName: "Test",
            enable: true
        })];
        spyOn(cryptosService, "deleteCryptos").and.returnValue(throwError({ status: 404 }));
        component.deleteCrypto(event, 0);
        expect().nothing();
    });

    it("should run #cancelCrypto()", async () => {
        const event = new MouseEvent("submit");
        component.cryptos = [new Cryptocurrencies({
            _id: null,
            name: "Test",
            logo: "Test",
            shortName: "Test",
            enable: true
        })];
        component.cancelCrypto(event, 0);
        expect(isEqual(component.cryptos, [])).toBeTruthy();
    });

});
