import { Component } from "@angular/core";
import { UserService } from "src/app/common/services/users.service";

/**
 * Entry component of application.
 */
@Component({
    selector: "app-root",
    templateUrl: "./app.component.html",
    styleUrls: ["./app.component.scss"]
})

export class AppComponent {

    /**
     * Constructor of AppComponent.
     * @param userService User service to init session.
     */
    constructor(private userService: UserService) {
        this.userService.initSession();
    }

}
