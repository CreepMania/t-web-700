import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { GuidedTourModule, GuidedTourService } from "ngx-guided-tour";
import { ToastrModule } from "ngx-toastr";
import { AppRoutingModule } from "src/app/app-routing.module";
import { AppComponent } from "src/app/app.component";
import { AppCommonModule } from "src/app/common/common.module";
import { ComponentsModule } from "src/app/components/components.module";

@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        AppRoutingModule,
        AppCommonModule,
        ComponentsModule,
        ToastrModule.forRoot(),
        GuidedTourModule
    ],
    providers: [
        GuidedTourService
    ],
    bootstrap: [
        AppComponent
    ]
})

export class AppModule { }
