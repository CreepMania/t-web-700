# CryptoWeb

[![pipeline status](https://gitlab.com/CreepMania/t-web-700/badges/master/pipeline.svg)](https://gitlab.com/CreepMania/t-web-700/-/commits/master) [![coverage report](https://gitlab.com/CreepMania/t-web-700/badges/master/coverage.svg)](https://gitlab.com/CreepMania/t-web-700/-/commits/master)

## Building the app
### Requirements
You will need the following requirements to build and launch the app:
```shell
 docker
 docker-compose
```

### Building
To build the app, simply execute the following command in your terminal:
> docker-compose build

## Running the app
To run the app, simply execute the following command in your termninal:
> docker-compose up -d


The app is by default launched on the 80 port.
Therefore, you can access the app through this link :
> localhost/home